Use back2gold.app to share expenses with friends, keep track of debts, and build up your network of trust where your currency is as good as gold

This is the frontend for back2gold.app, a PWA written in Elm that connects to a serverless firebase backend using ports.
