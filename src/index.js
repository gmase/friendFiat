import './style/main.scss';
import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getMessaging, isSupported } from 'firebase/messaging';
import { isMobile, firebaseConfig as config } from './firebase/config.js';
import {
    subscribeToUserChange,
    subscribeToSendNewProfile,
    subscribeToFetchBalance,
    //  subscribeToFetchManyUsers,
    subscribeToFetchFriends,
    subscribeToFetchManyBalances,
    subscribeToFetchManyTrans,
} from './firebase/user.js';
import {
    subscribeSendEmail,
    mySignInWithEmailLink,
} from './firebase/emailSignOn.js';
import {
    subscribeToSendMoney,
    subscribeToCopyToClipboard,
    subscribeToClaim,
    subscribeToSendInvite,
} from './firebase/transfer.js';
import {
    subscribeToSendRequest,
    subscribeToFetchManyRequest,
    subscribeToRespondToRequest,
    subscribeToFetchMyPendingRequest,
} from './firebase/request.js';

import { Elm } from './elm.js';

if (process.env.NODE_ENV !== 'production') {
    console.log('Looks like we are in development mode!');
}

if (!isMobile) {
    console.log('Looks like we are we are not in mobile!');
}

const disableApp = process.env.NODE_ENV === 'production' && !isMobile;
if (disableApp) {
    var span = document.createElement('span');
    span.innerHTML = 'back2gold only works on mobile devices';
    document.getElementById('elm').appendChild(span);
} else {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
            .register('/firebase-messaging-sw.js')
            .then(registration => {
                console.log('Service worker registration succeeded');
            })
            .catch(error => {
                console.log('Service worker registration failed:', error);
            });
    } else {
        console.log('Service workers are not supported.');
    }

    setTimeout(() => {
        if (
            Notification.permission !== 'granted' &&
            Notification.permission !== 'denied'
        ) {
            Notification.requestPermission();
        }
    }, 4000);

    const app = Elm.Main.init({
        flags: {
            numberFormat: (Math.PI * -1000).toLocaleString(),
            userLang: navigator.language || navigator.userLanguage,
        },
    });
    const firebaseApp = initializeApp(config);
    const auth = getAuth(firebaseApp);
    const db = getDatabase(firebaseApp);
    isSupported()
        .then(supported => {
            if (supported) {
                console.log('messaging supported!');
            }
            subscribeToAll({
                elmApp: app,
                auth: auth,
                db: db,
                messaging: supported ? getMessaging(firebaseApp) : null,
            });
        })
        .catch(error => {
            console.log('messaging not supported :(');
            subscribeToAll({
                elmApp: app,
                auth: auth,
                db: db,
                messaging: null,
            });
        });
}

function subscribeToAll(params) {
    mySignInWithEmailLink(params);
    subscribeToUserChange(params);
    subscribeSendEmail(params);
    subscribeToSendNewProfile(params);
    subscribeToSendMoney(params);
    subscribeToCopyToClipboard(params);
    subscribeToClaim(params);
    subscribeToFetchBalance(params);
    //subscribeToFetchManyUsers(params);
    subscribeToFetchFriends(params);
    subscribeToFetchManyBalances(params);
    subscribeToSendInvite(params);
    subscribeToFetchManyTrans(params);
    subscribeToSendRequest(params);
    subscribeToFetchManyRequest(params);
    subscribeToRespondToRequest(params);
    subscribeToFetchMyPendingRequest(params);
}
