module I18n exposing (Language(..), Texto(..), decodeLang, itext, showLanguageIcon)

import Element exposing (Element, el)
import Json.Decode as Decode exposing (string, succeed)


type Language
    = Es
    | En
    | Fr


decodeLang : Decode.Value -> Language
decodeLang json =
    case Decode.decodeValue langDecoder json of
        Ok value ->
            value

        Err msg ->
            En


langDecoder : Decode.Decoder Language
langDecoder =
    Decode.field "userLang" Decode.string
        |> Decode.andThen
            (\str ->
                case str |> String.split "-" of
                    "es" :: _ ->
                        succeed Es

                    "en" :: _ ->
                        succeed En

                    "fr" :: _ ->
                        succeed Fr

                    _ ->
                        succeed En
            )


showLanguageIcon : Language -> Bool -> Element msg
showLanguageIcon lang _ =
    case lang of
        Es ->
            el [] (Element.text "🇪🇸")

        Fr ->
            el [] (Element.text "🇫🇷")

        En ->
            el [] (Element.text "🇬🇧")


type Texto
    = Balance
    | MonedasDeOtros
    | MonedaEmitida
    | Tienes
    | UltimasTransferencias
    | DeEnvio String
    | AEnvio String
    | Peticiones
    | Pendientes
    | TePide String String
    | Pediste String String
    | AceptarRequest
    | RechazarRequest
    | PedirAContacto
    | Pedir
    | IrAPedir
    | Cancelar
    | Contactos
    | NuevaInvitacion
    | Copiar
    | Copiado
    | DeTuMoneda
    | QuiereSerTuAmigo String String
    | EnviarDinero
    | AContacto
    | Cheque
    | DetalleEnviar
    | DetalleDe String String
    | BorrarMensaje
    | Enviar
    | IrAEnviar
    | ConfirmarEnvio
    | Reclamar
    | ImporteSeAnadira
    | ContactoSeAnadira
    | AceptarEnvio
    | ChequeReclamado
    | InvitacionAceptada
    | SinReclamacionesDeOtros
    | SinReclamacionesTuyas
    | Perfil
    | IntruduceTuNombre
    | ActualizarPerfil
    | Nombre
    | MonedaReferencia
    | IntroduceEmail
    | EnviarEmail
    | RecibirasEmail
    | EmailEnviado
    | Espanol
    | Frances
    | Ingles
    | Logeate
    | BienvenidaTitulo1
    | BienvenidaTitulo2
    | BienvenidaTitulo3
    | BienvenidaSubtitulo
    | ComoFunciona
    | ComoFuncionaSubtitulo
    | ParaQue
    | ParaQueSubtitulo
    | Para
    | Cantidad
    | CantidadGold
    | CantidadFiat
    | Concepto
    | SeleccionaContacto
    | TeHaEnviado String String String String
    | TransferenciaRealizada
    | ComparteLink
    | Volver


itext : Language -> String -> Texto -> String
itext lang fiatSymbol key =
    case lang of
        Es ->
            case key of
                Balance ->
                    "BALANCE"

                MonedasDeOtros ->
                    "Monedas de otros"

                MonedaEmitida ->
                    "Moneda emitida"

                Tienes ->
                    "Tienes"

                UltimasTransferencias ->
                    "Últimas transferencias"

                DeEnvio sender ->
                    " De " ++ sender

                AEnvio receiver ->
                    " A " ++ receiver

                Peticiones ->
                    "PETICIONES"

                Pendientes ->
                    "Pendientes"

                TePide asker amount ->
                    asker ++ " te solicita " ++ amount

                Pediste from amount ->
                    "solicitaste " ++ amount ++ " a " ++ from

                AceptarRequest ->
                    "Aceptar"

                RechazarRequest ->
                    "Rechazar"

                PedirAContacto ->
                    "Pedir a un contacto"

                Pedir ->
                    "Pedir!"

                IrAPedir ->
                    "pedir"

                Cancelar ->
                    "Cancelar"

                Contactos ->
                    "CONTACTOS"

                NuevaInvitacion ->
                    "Generar nueva invitación"

                Copiar ->
                    "Copiar"

                Copiado ->
                    "¡Copiado!"

                DeTuMoneda ->
                    "De tu moneda"

                QuiereSerTuAmigo name link ->
                    name ++ " quiere ser tu amigo en back2gold.app - Sigue este link: " ++ link

                EnviarDinero ->
                    "ENVIAR DINERO"

                AContacto ->
                    "A un contacto"

                Cheque ->
                    "Cheque al portador"

                DetalleEnviar ->
                    "Detalle a enviar:"

                DetalleDe amount issuer ->
                    amount ++ " de " ++ issuer

                BorrarMensaje ->
                    "Borrar mensaje"

                Enviar ->
                    "Enviar!"

                IrAEnviar ->
                    "enviar"

                ConfirmarEnvio ->
                    "¿Estás seguro de querer realizar el envío?"

                Reclamar ->
                    "Reclamar"

                ImporteSeAnadira ->
                    "El importe de la transacción se añadirá a tu balance"

                ContactoSeAnadira ->
                    "Se añadirá a tus contactos"

                AceptarEnvio ->
                    "Aceptar"

                ChequeReclamado ->
                    "Cheque reclamado, visita tu balance para comprobarlo"

                InvitacionAceptada ->
                    "Invitación aceptada, visita tus contactos para comprobarlo"

                SinReclamacionesDeOtros ->
                    "Ninguna petición de tus contactos pendiente"

                SinReclamacionesTuyas ->
                    "Ninguna petición tuya pendiente"

                Perfil ->
                    "PERFIL"

                IntruduceTuNombre ->
                    "Actualiza tu nombre para que tus contactos puedan encontrarte"

                ActualizarPerfil ->
                    "Guardar"

                Nombre ->
                    "Nombre"

                MonedaReferencia ->
                    "Moneda de referencia"

                IntroduceEmail ->
                    "Introduce tu email para entrar"

                EnviarEmail ->
                    "Enviar email"

                RecibirasEmail ->
                    "Recibirás un email con un link para logearte"

                EmailEnviado ->
                    "Te hemos enviado un email con un link que debes copiar en tu navegador para logearte"

                Espanol ->
                    "Español"

                Frances ->
                    "Francés"

                Ingles ->
                    "Inglés"

                Logeate ->
                    "Iniciar sesión"

                BienvenidaTitulo1 ->
                    "Emite tu propia moneda en base al precio del"

                BienvenidaTitulo2 ->
                    " or☉"

                BienvenidaTitulo3 ->
                    ""

                BienvenidaSubtitulo ->
                    "Comparte gastos con tus amigos, lleva la cuenta de tus deudas y construye una red de confianza donde tu moneda vale tanto como el oro"

                ComoFunciona ->
                    "¿Cómo funciona?"

                ComoFuncionaSubtitulo ->
                    "Cada usuario tiene su propia moneda y puede crear tanto de su dinero como quiera para enviárselo a sus contactos. Todas las monedas tienen su equivalencia en base a gramos de oro, pero puedes ver los importes en tu moneda de referencia ($, €...) para tener una mejor idea de las cantidades"

                ParaQue ->
                    "¿Para qué puedo usarlo?"

                ParaQueSubtitulo ->
                    "Compartir gastos es solo el principio, también podrás hacer préstamos,  hacer regalos de saldo en tu moneda, conseguir bonos para gastar en comercios... ¡las posibilidades son infinitas!"

                Para ->
                    "Para"

                Cantidad ->
                    "Cantidad"

                CantidadGold ->
                    "mg"

                CantidadFiat ->
                    fiatSymbol

                Concepto ->
                    "Concepto"

                SeleccionaContacto ->
                    "Selecciona un contacto"

                TeHaEnviado from qty concept linkText ->
                    from ++ " te ha enviado: " ++ qty ++ " por \"" ++ concept ++ "\" - Sigue este link y reclama tu dinero: " ++ linkText

                TransferenciaRealizada ->
                    "Transferencia realizada!\n"

                ComparteLink ->
                    "Deja que el destinatario escanee el código QR o comparte el siguiente mensaje él"

                Volver ->
                    "volver"

        Fr ->
            case key of
                Balance ->
                    "BILAN"

                MonedasDeOtros ->
                    "Devises de mes contacts"

                MonedaEmitida ->
                    "Mes devises émises"

                Tienes ->
                    "Vous avez"

                UltimasTransferencias ->
                    "Derniers transferts réalisés"

                DeEnvio sender ->
                    " De " ++ sender

                AEnvio receiver ->
                    " À " ++ receiver

                Peticiones ->
                    "DEMANDES"

                Pendientes ->
                    "En attente"

                TePide asker amount ->
                    asker ++ " vous demande " ++ amount

                Pediste from amount ->
                    "Vous avez demandé " ++ amount ++ " à " ++ from

                AceptarRequest ->
                    "Accepter"

                RechazarRequest ->
                    "Refuser"

                PedirAContacto ->
                    "Demander à un contact"

                Pedir ->
                    "Demander !"

                IrAPedir ->
                    "demande"

                Cancelar ->
                    "Annuler"

                Contactos ->
                    "Contacts"

                NuevaInvitacion ->
                    "Nouvelle invitation"

                Copiar ->
                    "Copier"

                Copiado ->
                    "Copied!"

                DeTuMoneda ->
                    "De ma devise"

                QuiereSerTuAmigo name link ->
                    name ++ " veut être ton ami dans back2gold.app - Suivez ce lien: " ++ link

                EnviarDinero ->
                    "ENVOYER L'ARGENT"

                AContacto ->
                    "À un contact"

                Cheque ->
                    "Chèque au porteur"

                DetalleEnviar ->
                    "Détail:"

                DetalleDe amount issuer ->
                    amount ++ " de " ++ issuer

                BorrarMensaje ->
                    "Effacer le message"

                Enviar ->
                    "Envoyer!"

                IrAEnviar ->
                    "envoyer"

                ConfirmarEnvio ->
                    "Êtes-vous sûr de vouloir effectuer l'envoi?"

                Reclamar ->
                    "Réclamer"

                ImporteSeAnadira ->
                    "Le transfert figurera dans votre bilan."

                ContactoSeAnadira ->
                    "Sera ajouté.e à ma liste de contacts"

                AceptarEnvio ->
                    "Accepter"

                ChequeReclamado ->
                    "Chèque encaissé, désormais visible sur mon bilan"

                InvitacionAceptada ->
                    "Invitation acceptée"

                SinReclamacionesDeOtros ->
                    "Aucune demande de vos contacts en attente"

                SinReclamacionesTuyas ->
                    "Aucune demande en attente de votre part"

                Perfil ->
                    "PROFIL"

                IntruduceTuNombre ->
                    "Update your name so your friends can find you"

                ActualizarPerfil ->
                    "Enregistrer"

                Nombre ->
                    "Nom"

                MonedaReferencia ->
                    "Fiat"

                IntroduceEmail ->
                    "Entrez votre adresse mail pour accéder"

                EnviarEmail ->
                    "Envoyer le mail"

                RecibirasEmail ->
                    "Vous recevrez un mail avec le lien à suivre pour vous connecter."

                EmailEnviado ->
                    "Nous vous avons envoyé un mail avec le lien à suivre pour vous connecter."

                Espanol ->
                    "Espagnol"

                Frances ->
                    "Français"

                Ingles ->
                    "Anglais"

                Logeate ->
                    "Sign in"

                BienvenidaTitulo1 ->
                    "Emite tu propia moneda en base al precio del"

                BienvenidaTitulo2 ->
                    " oro"

                BienvenidaTitulo3 ->
                    ""

                BienvenidaSubtitulo ->
                    "Partagez vos dépenses avec vos amis, faites le suivi de vos dettes et construisez un réseau de confiance où votre devise vaut autant que l'or"

                ComoFunciona ->
                    "Comment ça marche?"

                ComoFuncionaSubtitulo ->
                    "Each user has its own currency and can issue money at any time to send to his friends. Every currency has an equivalence to gold grams, but you can use a reference currency ($, €...) to better understand amounts"

                ParaQue ->
                    "A quoi puis-je l'utiliser?"

                ParaQueSubtitulo ->
                    "Le partage des dépenses n'est qu'un début, vous pouvez aussi faire des prêts, faire des cadeaux de solde dans votre devise, obtenir des bonus à dépenser dans les magasins... les possibilités sont infinies !"

                Para ->
                    "Pour"

                Cantidad ->
                    "Quantité"

                CantidadGold ->
                    "mg"

                CantidadFiat ->
                    fiatSymbol

                Concepto ->
                    "Message"

                SeleccionaContacto ->
                    "Sélectionner un contact"

                TeHaEnviado from qty concept linkText ->
                    from ++ " m'a envoyé: " ++ qty ++ " pour \"" ++ concept ++ "\" - Cliquer sur ce lien pour réclamer mon argent : " ++ linkText

                TransferenciaRealizada ->
                    "Transfert effectué !\n"

                ComparteLink ->
                    "Envoyer ce message au destinataire du chèque"

                Volver ->
                    "back"

        En ->
            case key of
                Balance ->
                    "BALANCE"

                MonedasDeOtros ->
                    "Fiat from others"

                MonedaEmitida ->
                    "Issued money"

                Tienes ->
                    "You have"

                UltimasTransferencias ->
                    "Last movements"

                DeEnvio sender ->
                    " From " ++ sender

                AEnvio receiver ->
                    " To " ++ receiver

                Peticiones ->
                    "REQUESTS"

                Pendientes ->
                    "Pending"

                TePide asker amount ->
                    asker ++ " asks for " ++ amount

                Pediste from amount ->
                    "you asked " ++ from ++ " for " ++ amount

                AceptarRequest ->
                    "Accept"

                RechazarRequest ->
                    "Reject"

                PedirAContacto ->
                    "Ask a friend"

                Pedir ->
                    "Ask!"

                IrAPedir ->
                    "ask"

                Cancelar ->
                    "Cancel"

                Contactos ->
                    "FRIENDS"

                NuevaInvitacion ->
                    "Create new invite"

                Copiar ->
                    "Copy"

                Copiado ->
                    "Copied!"

                DeTuMoneda ->
                    "Of your fiat"

                QuiereSerTuAmigo name link ->
                    name ++ " wants to be your friend in back2gold.app - Follow this link: " ++ link

                EnviarDinero ->
                    "SEND MONEY"

                AContacto ->
                    "To a contact"

                Cheque ->
                    "Bearer check"

                DetalleEnviar ->
                    "Detail:"

                DetalleDe amount issuer ->
                    amount ++ " from " ++ issuer

                BorrarMensaje ->
                    "Delete message"

                Enviar ->
                    "Send!"

                IrAEnviar ->
                    "send"

                ConfirmarEnvio ->
                    "Really send?"

                Reclamar ->
                    "Claim"

                ImporteSeAnadira ->
                    "The money will be added to your balance"

                ContactoSeAnadira ->
                    "He/she'll be added to your friend list"

                AceptarEnvio ->
                    "Accept"

                ChequeReclamado ->
                    "Claimed check, visit balance to see it"

                InvitacionAceptada ->
                    "Invite accepted"

                SinReclamacionesDeOtros ->
                    "No pending requests from others"

                SinReclamacionesTuyas ->
                    "No pending requests from you"

                Perfil ->
                    "PROFILE"

                IntruduceTuNombre ->
                    "Update your name so your friends can find you"

                ActualizarPerfil ->
                    "Save"

                Nombre ->
                    "Name"

                MonedaReferencia ->
                    "Reference currency"

                IntroduceEmail ->
                    "Enter your email"

                EnviarEmail ->
                    "Send email"

                RecibirasEmail ->
                    "You''ll get an email with a link to log in"

                EmailEnviado ->
                    "We've sent you an email with a link you must open in your browser in order to log in"

                Espanol ->
                    "Spanish"

                Frances ->
                    "French"

                Ingles ->
                    "English"

                Logeate ->
                    "Sign in"

                BienvenidaTitulo1 ->
                    "Issue your own "

                BienvenidaTitulo2 ->
                    "g☉ld"

                BienvenidaTitulo3 ->
                    " based currency"

                BienvenidaSubtitulo ->
                    "Share expenses with friends, keep track of debts, and build up your network of trust where your currency is as good as gold"

                ComoFunciona ->
                    "How does it work?"

                ComoFuncionaSubtitulo ->
                    "Each user has its own currency and can issue money at any time to send to his friends. Every currency has an equivalence to gold grams, but you can use a reference currency ($, €...) to better understand amounts"

                ParaQue ->
                    "What can I use it for?"

                ParaQueSubtitulo ->
                    "Sharing expenses is just the beginning, you'll be able to leand money, give some of your money as present, get discount checks to spend in shops...Unlimited possibilties!"

                Para ->
                    "To"

                Cantidad ->
                    "Amount"

                CantidadGold ->
                    "mg"

                CantidadFiat ->
                    fiatSymbol

                Concepto ->
                    "Subject"

                SeleccionaContacto ->
                    "Select contact"

                TeHaEnviado from qty concept linkText ->
                    from ++ " te ha enviado: " ++ qty ++ " por \"" ++ concept ++ "\" - Sigue este link y reclama tu dinero: " ++ linkText

                TransferenciaRealizada ->
                    "Transfer executed!\n"

                ComparteLink ->
                    "Share this message with the recipient of the check"

                Volver ->
                    "back"
