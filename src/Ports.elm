port module Ports exposing (..)

import Json.Decode as Decode exposing (list, string, succeed)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode
import Ledger exposing (MoneyPersonsWithTotal, moneyDecoder)
import Money exposing (MoneyPerson)


type alias ProfileToUpdate =
    { name : String
    , language : String
    , fiat: String
    }


type alias EmailAndLink =
    { email : String
    , link : String
    }


type alias Transfer =
    { to : String
    , amount : List MoneyPerson
    , status : String
    , kind : String
    , addFriendSender : Bool
    , concept : String
    }


type alias TransferData =
    { id : String
    , to : String
    , from : String
    , amount : List MoneyPerson
    , status : String
    , kind : String
    , concept : String
    }


type alias RequestToPort =
    { to : String
    , concept : String
    , amountFrom : MoneyPersonsWithTotal
    , amountTo : MoneyPersonsWithTotal
    , status : String
    }


type Status
    = Pending
    | Accepted
    | Rejected


transDecoder : Decode.Decoder TransferData
transDecoder =
    succeed TransferData
        |> required "id" string
        |> required "to" string
        |> required "from" string
        |> required "amount" (list moneyDecoder)
        |> required "status" string
        |> required "kind" string
        |> required "concept" string



-- User & sign in
-- IN


port receiveUser : (Encode.Value -> msg) -> Sub msg


port receiveManyUsers : (Encode.Value -> msg) -> Sub msg


port receiveFriends : (Encode.Value -> msg) -> Sub msg


port noUser : (Encode.Value -> msg) -> Sub msg



-- OUT


port sendEmail : EmailAndLink -> Cmd msg


port fetchMyData : String -> Cmd msg


port sendNewProfile : ProfileToUpdate -> Cmd msg


port fetchManyUsers : List String -> Cmd msg


port fetchFriends : List String -> Cmd msg



-- Transfer
-- IN


port receiveTransferId : (Encode.Value -> msg) -> Sub msg


port receiveInviteId : (Encode.Value -> msg) -> Sub msg


port receiveManyTrans : (Encode.Value -> msg) -> Sub msg


port receiveManyRequests : (Encode.Value -> msg) -> Sub msg


port receiveMyPendingRequests : (Encode.Value -> msg) -> Sub msg



-- OUT


port sendMoney : Transfer -> Cmd msg


port sendRequest : RequestToPort -> Cmd msg


port fetchManyRequests : String -> Cmd msg


port fetchMyPendingRequests : String -> Cmd msg


type alias RequestResponse =
    { id : String
    , status : String
    }


port respondToRequest : RequestResponse -> Cmd msg


port sendInvite : Transfer -> Cmd msg


port copyToClipboard : () -> Cmd msg


type alias Claim =
    { transId : String
    , addFriend : Bool
    }


port claim : Claim -> Cmd msg


port fetchManyTrans : List String -> Cmd msg



-- Balances
-- IN


port receiveBalance : (Encode.Value -> msg) -> Sub msg


port receiveManyBalances : (Encode.Value -> msg) -> Sub msg



-- OUT


port fetchBalance : String -> Cmd msg


port fetchManyBalances : List String -> Cmd msg


-- RATES
--IN
port receiveRates : (Encode.Value -> msg) -> Sub msg
