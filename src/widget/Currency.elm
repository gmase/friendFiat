--DEPRECATED


module Currency exposing (Model, displayOrHide, init, showInteractiveCurrency)

import Element exposing (alignRight, el, paragraph)
import Element.Font as Font
import Element.Input as Input
import Money exposing (MoneyPerson)
import Style


type alias Model =
    { have : MoneyPerson
    , display : Bool
    }


init : MoneyPerson -> Model
init have =
    { have = have
    , display = False
    }


displayOrHide : Maybe Model -> Maybe Model
displayOrHide model =
    case model of
        Nothing ->
            Nothing

        Just smt ->
            Just { smt | display = not smt.display }


showInteractiveCurrency event extraFormat toText toDetail model =
    Input.button [ alignRight ]
        { onPress = event
        , label =
            paragraph []
                [ el (Font.bold :: extraFormat) (toText model.have)
                , showInfoOrDetail model.display (toDetail model.have)
                ]
        }


showInfoOrDetail display detailText =
    if display then
        el Style.infoIcon (Element.text detailText)

    else
        el Style.infoIcon (Element.text "i")
