module SendWidget exposing (Model, Msg(..), clearQtyConcept, clearTo, init, printSendCheck, printSendToContact, update, updateWithUser)

import Dict exposing (empty)
import Element exposing (Element, alignLeft, column, el, fill, htmlAttribute, row, spacing, width)
import Element.Input as Input
import Gold exposing (maxGold)
import Html.Attributes as Attr exposing (value)
import I18n exposing (Texto(..))
import PrioritySelector
import Style
import User exposing (User, userSorter)


type GoldOrFiat
    = Gold
    | Fiat


type alias Model =
    { to : Maybe String
    , qty : String
    , qtyFiat : String
    , concept : String
    , friendDict : User.FriendDict
    , toSelector : PrioritySelector.Model
    , goldOrFiat : GoldOrFiat
    }


init : Model
init =
    { to = Nothing
    , qty = ""
    , qtyFiat = ""
    , concept = ""
    , friendDict = empty
    , toSelector = PrioritySelector.init []
    , goldOrFiat = Fiat
    }


updateWithUser : Model -> User -> Model
updateWithUser model user =
    let
        friends =
            user.friends |> List.sortWith (userSorter user.userId user.balanceDict)
    in
    { model
        | friendDict = user.friendDict
        , toSelector =
            friends
                |> List.map (getOption user.friendDict (List.length friends == 1))
                |> PrioritySelector.init
        , to =
            case friends of
                x :: [] ->
                    Just x.id

                _ ->
                    model.to
    }


getOption : User.FriendDict -> Bool -> User.Friend -> ( String, String, Bool )
getOption friendDict setSelected friend =
    ( friend.id, User.getFriendNameFromDict friendDict friend.id, setSelected )


clearQtyConcept : Model -> Model
clearQtyConcept model =
    { model | qty = "", qtyFiat = "", concept = "" }


clearTo : Model -> Model
clearTo model =
    { model
        | to =
            if Dict.size model.friendDict == 1 then
                model.to

            else
                Nothing
    }



-- UPDATE


type Msg
    = NoOp
    | ChangeQty String String
    | ChangeQtyFiat String String
    | ChangeConcept String
    | ChangeFiat
    | GotToSelectorMsg PrioritySelector.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ChangeQty value otherValue ->
            ( { model | qty = value, qtyFiat = otherValue }, Cmd.none )

        ChangeQtyFiat value otherValue ->
            ( { model | qtyFiat = value, qty = otherValue }, Cmd.none )

        ChangeConcept value ->
            ( { model | concept = value }, Cmd.none )

        ChangeFiat ->
            ( { model
                | goldOrFiat =
                    case model.goldOrFiat of
                        Gold ->
                            Fiat

                        Fiat ->
                            Gold
              }
            , Cmd.none
            )

        GotToSelectorMsg subMsg ->
            let
                selector =
                    PrioritySelector.update subMsg model.toSelector
            in
            ( { model | toSelector = selector, to = PrioritySelector.getSingleSelection selector }, Cmd.none )


printSendToContact : (Bool -> Float -> Float) -> (Texto -> String) -> Model -> Element Msg
printSendToContact converter itext model =
    column
        Style.pageFormat
        [ targetSelector itext model
        , quantityInput converter itext model.goldOrFiat model.qty model.qtyFiat
        , conceptInput itext model.concept
        ]


targetSelector : (Texto -> String) -> Model -> Element Msg
targetSelector itext model =
    column [ width fill, spacing 10 ]
        [ itext Para |> Element.text |> el [ alignLeft ]
        , PrioritySelector.view
            model.toSelector
            |> Element.map GotToSelectorMsg
        ]


quantityInput : (Bool -> Float -> Float) -> (Texto -> String) -> GoldOrFiat -> String -> String -> Element Msg
quantityInput converter itext goldOrFiat qty qtyFiat =
    column [ width fill, spacing 10 ]
        [ el [ alignLeft ] (Element.text <| itext Cantidad)
        , row [ width fill, spacing 20 ]
            [ case goldOrFiat of
                Fiat ->
                    Input.text
                        [ htmlAttribute (Attr.type_ "number") ]
                        { label = Input.labelRight [] (Element.text <| itext CantidadFiat)
                        , placeholder = Nothing
                        , text = qtyFiat
                        , onChange = doChangeQtyFiat (converter True)
                        }

                Gold ->
                    Input.text
                        [ htmlAttribute (Attr.type_ "number") ]
                        { label = Input.labelRight [] (Element.text <| itext CantidadGold)
                        , placeholder = Nothing
                        , text = qty
                        , onChange = doChangeQty (converter False)
                        }
            , Input.button Style.buttonChangeCurrency { onPress = Just ChangeFiat, label = Element.text <| "⇋" }
            ]
        ]



--⇵
-- ᐅ
-- ⥮
-- ⇋


conceptInput : (Texto -> String) -> String -> Element Msg
conceptInput itext concept =
    Input.text
        [ spacing 10 ]
        { label = Input.labelAbove [] (Element.text <| itext Concepto)
        , placeholder = Nothing
        , text = concept
        , onChange = ChangeConcept
        }



-- CHECK


printSendCheck : (Bool -> Float -> Float) -> (Texto -> String) -> Model -> Element Msg
printSendCheck converter itext model =
    column
        Style.pageFormat
        [ quantityInput converter itext model.goldOrFiat model.qty model.qtyFiat
        , conceptInput itext model.concept
        ]


doChangeQty : (Float -> Float) -> String -> Msg
doChangeQty converter newQty =
    let
        qtyGold =
            Maybe.withDefault 0 (String.toFloat newQty)
    in
    if
        (newQty |> String.any (\c -> not (Char.isDigit c)))
            || (round qtyGold > maxGold)
    then
        NoOp

    else
        qtyGold |> converter |> String.fromFloat |> ChangeQty (String.fromFloat qtyGold)


doChangeQtyFiat : (Float -> Float) -> String -> Msg
doChangeQtyFiat converter newQty =
    let
        qtyString =
            String.replace "," "." newQty

        qtyFiat =
            Maybe.withDefault 0 (String.toFloat qtyString)

        qtyGold =
            qtyFiat |> converter
    in
    if
        (qtyString |> String.any (\c -> c /= '.' && not (Char.isDigit c)))
            || (round qtyGold > maxGold)
    then
        NoOp

    else
        qtyGold |> String.fromFloat |> ChangeQtyFiat qtyString



--gold symbol ☉
