module PrioritySelector exposing (Model, Msg, getSingleSelection, init, update, view)

import Element exposing (Element, el, fill, html, paddingXY, spacing, width, wrappedRow)
import Element.Background as Background
import Element.Font as Font
import Element.Input as Input
import Html exposing (select)
import Html.Attributes exposing (value)
import Html.Events exposing (onInput)
import Style


getSingleSelection : Model -> Maybe String
getSingleSelection model =
    case model.options |> List.filter (\x -> x.selected) of
        x :: xs ->
            Just x.id

        _ ->
            Nothing



-- Option


type alias Option =
    { id : String
    , name : String
    , selected : Bool
    }


createOption : ( String, String, Bool ) -> Option
createOption ( id, name, selected ) =
    { id = id, name = name, selected = selected }



-- MODEL


type SelectType
    = Fast
    | AllOptions


type alias Model =
    { selectType : SelectType
    , options : List Option
    , multipleSelect : Bool
    }


init : List ( String, String, Bool ) -> Model
init options =
    { selectType = Fast
    , options = options |> List.map createOption
    , multipleSelect = False
    }



-- UPDATE


type Msg
    = ShowAll
    | Select String


update msg model =
    case msg of
        ShowAll ->
            { model | selectType = AllOptions, options = selectFirstOption model.options }

        Select option ->
            { model
                | options = model.options |> List.map (updateOption option model.multipleSelect)
            }


selectFirstOption options =
    case options of
        x :: xs ->
            { x | selected = True } :: xs

        [] ->
            []


updateOption selection multipleSelect oldOption =
    if oldOption.id == selection then
        { oldOption | selected = True }

    else if multipleSelect then
        oldOption

    else
        { oldOption | selected = False }



-- View


view : Model -> Element Msg
view model =
    case model.selectType of
        AllOptions ->
            model.options |> allSelector

        Fast ->
            (if List.length model.options > 3 then
                [ Input.button []
                    { onPress = Just ShowAll
                    , label = "+" |> Element.text |> el [ paddingXY 10 5, Background.color Style.cBG1, Font.color Style.colorGreen, Font.bold ]
                    }
                ]

             else
                []
            )
                |> List.append (renderFastOptions model.options)
                |> wrappedRow [ spacing 10 ]



-- PrioritySelector


renderFastOptions : List Option -> List (Element Msg)
renderFastOptions options =
    options
        |> getFastAccessOptions
        |> List.map renderOption


renderOption : Option -> Element Msg
renderOption option =
    Input.button (selectedOptionFormat option.selected)
        { onPress = Just (Select option.id)
        , label = option.name |> Element.text |> el []
        }


getFastAccessOptions : List Option -> List Option
getFastAccessOptions options =
    case options of
        [] ->
            []

        x :: y :: z :: xs ->
            [ x, y, z ]

        x :: y :: xs ->
            [ x, y ]

        x :: xs ->
            [ x ]



-- ALL OPTIONS


allSelector : List Option -> Element Msg
allSelector options =
    html
        (select
            (onInput Select :: Style.htmlSelect)
            (options |> List.map getSlowOption)
        )
        |> el [ width fill ]


getSlowOption : Option -> Html.Html msg
getSlowOption option =
    Html.option [ value option.id ]
        [ option.name |> Html.text ]



-- STYLE


selectedOptionFormat selected =
    if selected then
        Style.buttonContact

    else
        Style.buttonContactUnselected
