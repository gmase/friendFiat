module Main exposing (Model, Msg(..), init, main, update, view)

import Asset
import Balance
import Browser exposing (Document)
import Browser.Navigation as Nav
import Collect
import Dict exposing (empty, fromList, get)
import Element exposing (Element, alignRight, alpha, centerX, column, el, fill, height, htmlAttribute, image, inFront, minimum, moveLeft, moveUp, none, paddingXY, px, row, shrink, spaceEvenly, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onClick)
import Element.Font as Font
import FormatNumber.Locales exposing (Locale)
import Friends
import Gold exposing (GoldAndFiat, Rates, convert, decodeLocale, decodeRates, goldFiatToParagraph, printGold)
import Html exposing (Html)
import Html.Attributes exposing (style)
import I18n exposing (Language(..), Texto(..), decodeLang, itext)
import Json.Decode as Decode
import Ledger exposing (Ledger, decodeManyLedger)
import Ports exposing (fetchBalance, fetchFriends, fetchManyBalances, fetchManyRequests, fetchManyTrans, fetchMyPendingRequests, noUser, receiveBalance, receiveFriends, receiveInviteId, receiveManyBalances, receiveManyRequests, receiveManyTrans, receiveMyPendingRequests, receiveRates, receiveTransferId, receiveUser)
import Pref
import Profile
import Request exposing (Request, decodeManyRequests)
import Send
import SignOn
import String exposing (dropLeft, left, toLower)
import Style exposing (fontSizeOfDefault)
import Url exposing (Url)
import Url.Parser exposing ((<?>), Parser, map, oneOf, parse, s)
import Url.Parser.Query as Query
import User exposing (TransDict, User, decodeManyTrans, decodeUser, decodeUsers, friendAndRelationsFromUser, updateUserWithPrefs, updateUserWithRates)


decodeTransferId : Decode.Value -> String
decodeTransferId json =
    case Decode.decodeValue Decode.string json of
        Ok value ->
            value

        Err msg ->
            ""



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ receiveUser (decodeUser model.locale >> GotUser)
        , receiveFriends (decodeUsers model.locale >> GotFriendList)
        , noUser ((\_ -> SignOn.GotNoUser) >> GotSignOnMsg)
        , receiveTransferId (decodeTransferId >> Send.ReceivedTransferId >> GotSendMsg)
        , receiveInviteId (decodeTransferId >> Friends.ReceivedInviteId >> GotFriendsMsg)
        , receiveBalance (decodeManyLedger >> GotBalanceList)
        , receiveManyBalances (decodeManyLedger >> GotBalanceList)
        , receiveManyTrans (decodeManyTrans >> GotTransList)
        , receiveManyRequests (decodeManyRequests >> GotRequestList Request.PendingRequests)
        , receiveMyPendingRequests (decodeManyRequests >> GotRequestList Request.MyRequests)
        , receiveRates (decodeRates >> GotRates)
        ]


main : Program Decode.Value Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        , subscriptions = subscriptions
        , update = update
        , view = view
        }



-- MODEL


type alias SessionModel =
    { profileModel : Profile.Model
    , sendModel : Send.Model
    , balanceModel : Balance.Model
    , friendsModel : Friends.Model
    , requestModel : Request.Model
    }


type Page
    = Profile Profile.Model
    | Send Send.Model
    | SignOn SignOn.Model
    | Collect Collect.Model
    | Balance Balance.Model
    | Friends Friends.Model
    | Request Request.Model



-- type alias Model =
--     { key : Nav.Key
--     , url : Url
--     , appData : AppData
--     , havePendingRequests : Bool -- TODO: remove
--     , locale: Locale -- TODO: remove
--     }
-- type CollectUrl = Maybe String
-- type AppData
--     = NoUser CollectUrl Page --
--     | PageWithUser User Page SessionModel


type alias Model =
    { pageModel : Page
    , key : Nav.Key
    , url : Url
    , user : Maybe User
    , locale : Locale
    , navLang : Language
    , session : SessionModel
    , collectUrl : Maybe String
    , havePendingRequests : Bool
    }


pageParserWithModel : Model -> Parser (Page -> a) a
pageParserWithModel model =
    case model.user of
        Nothing ->
            oneOf
                [ map (Send model.session.sendModel) (s "send")
                , map (SignOn (SignOn.init model.navLang model.collectUrl)) (s "signOn")
                , map (Balance model.session.balanceModel) (s "balance")
                , map (Friends model.session.friendsModel) (s "friends")
                , map (Request (Request.modelWithoutTo model.session.requestModel)) (s "request")
                , map (Profile (Profile.initWithUser model.user)) (s "profile")
                , map (Collect << Collect.init Collect.Payment) (s "collect" <?> Query.string "t")
                , map (Collect << Collect.init Collect.Invite) (s "invite" <?> Query.string "t")
                ]

        Just smt ->
            oneOf
                [ map (Send (Send.modelWithoutTo model.session.sendModel)) (s "send")
                , map (Send model.session.sendModel) (s "signOn")
                , map (Balance model.session.balanceModel) (s "balance")
                , map (Friends model.session.friendsModel) (s "friends")
                , map (Request (Request.modelWithoutTo model.session.requestModel)) (s "request")
                , map (Profile (Profile.initWithUser (Just smt))) (s "profile")
                , map (Collect << Collect.init Collect.Payment) (s "collect" <?> Query.string "t")
                , map (Collect << Collect.init Collect.Invite) (s "invite" <?> Query.string "t")
                ]


getUrlTransferIdParser : String -> Parser (Maybe String -> a) a
getUrlTransferIdParser keyword =
    s keyword <?> Query.string "t"


getUrlParserMap keyword =
    map
        (\x ->
            case x of
                Nothing ->
                    Nothing

                Just smth ->
                    Just ("/" ++ keyword ++ "?t=" ++ smth)
        )
        (getUrlTransferIdParser keyword)


continuationUrlParser : Parser (Maybe String -> a) a
continuationUrlParser =
    oneOf
        [ getUrlParserMap "collect"
        , getUrlParserMap "invite"
        ]


init : Decode.Value -> Url -> Nav.Key -> ( Model, Cmd Msg )
init json url navKey =
    let
        collectUrl =
            case parse continuationUrlParser url of
                Nothing ->
                    Nothing

                Just smth ->
                    smth

        navLang =
            decodeLang json
    in
    ( { pageModel =
            SignOn (SignOn.init navLang collectUrl)
      , key = navKey
      , url = url
      , user = Nothing
      , collectUrl = collectUrl
      , locale = decodeLocale json
      , navLang = navLang
      , session =
            { profileModel = Profile.initWithUser Nothing
            , sendModel = Send.init
            , balanceModel = Balance.init Nothing
            , friendsModel = Friends.init []
            , requestModel = Request.init
            }
      , havePendingRequests = False
      }
    , Nav.pushUrl navKey (Url.toString { url | path = "/signOn" })
    )



-- UPDATE


type Msg
    = UrlChanged Url.Url
    | LinkClicked Browser.UrlRequest
    | GoTo Screen
    | GotUser (Maybe User)
    | GotRates (Maybe Rates)
    | GotFriendList (List User)
    | GotBalanceList (List Ledger)
    | GotTransList TransDict
    | GotProfileMsg Profile.Msg
    | GotSendMsg Send.Msg
    | GotSignOnMsg SignOn.Msg
    | GotCollectMsg Collect.Msg
    | GotBalanceMsg Balance.Msg
    | GotFriendsMsg Friends.Msg
    | GotRequestMsg Request.Msg
    | GotRequestList Request.RequestListKind (List Request)


type Screen
    = SendScreen
    | ProfileScreen
    | SignOnScreen
    | CollectScreen
    | BalanceScreen
    | FriendsScreen
    | RequestScreen


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            let
                pageModel =
                    Maybe.withDefault model.pageModel (parse (pageParserWithModel model) url)

                ( pageModel2, initCmd ) =
                    case model.user of
                        Nothing ->
                            ( pageModel, Cmd.none )

                        Just smth ->
                            case pageModel of
                                Collect collectModel ->
                                    Tuple.mapFirst (\m -> Collect m) (Collect.doClaim collectModel)

                                Friends friendsModel ->
                                    let
                                        friendIds =
                                            List.map (\f -> f.id) smth.friends
                                    in
                                    ( Friends friendsModel, fetchManyBalances friendIds )

                                Request requestModel ->
                                    ( Request requestModel
                                    , Cmd.batch
                                        [ fetchManyRequests "nothing"
                                        , fetchMyPendingRequests "nothing"
                                        ]
                                    )

                                Balance balanceModel ->
                                    ( Balance balanceModel, Cmd.map GotBalanceMsg Balance.initCmd )

                                _ ->
                                    ( pageModel, Cmd.none )
            in
            ( { model | url = url, pageModel = pageModel2 }
            , initCmd
            )

        GoTo screen ->
            let
                urlWithQuery =
                    model.url

                url =
                    { urlWithQuery | query = Nothing }
            in
            case screen of
                SendScreen ->
                    ( model
                    , Nav.pushUrl model.key (Url.toString { url | path = "/send" })
                    )

                ProfileScreen ->
                    ( model
                    , Nav.pushUrl model.key (Url.toString { url | path = "/profile" })
                    )

                SignOnScreen ->
                    ( model
                    , Nav.pushUrl model.key (Url.toString { urlWithQuery | path = "/signOn" })
                    )

                CollectScreen ->
                    ( model
                    , Nav.pushUrl model.key (Url.toString { urlWithQuery | path = "/collect" })
                    )

                BalanceScreen ->
                    ( model
                    , Nav.pushUrl model.key (Url.toString { url | path = "/balance" })
                    )

                FriendsScreen ->
                    ( model
                    , Nav.pushUrl model.key (Url.toString { url | path = "/friends" })
                    )

                RequestScreen ->
                    ( model
                    , Nav.pushUrl model.key (Url.toString { url | path = "/request" })
                    )

        GotUser user ->
            let
                newModel =
                    updateModelWithUser model user

                goBackCmd =
                    case model.user of
                        Nothing ->
                            Nav.back model.key 1

                        _ ->
                            Cmd.none

                friendIds =
                    case user of
                        Nothing ->
                            []

                        Just smth ->
                            List.map (\f -> f.id) smth.friends
            in
            ( newModel
            , Cmd.batch
                [ fetchFriends friendIds
                , fetchBalance "userId"
                , fetchManyBalances friendIds
                , goBackCmd
                ]
            )

        GotRates rates ->
            let
                newModel =
                    updateModelWithRates model rates
            in
            ( newModel
            , Cmd.none
            )

        GotFriendList friends ->
            let
                oldUser =
                    model.user

                oldFriendDict =
                    case oldUser of
                        Nothing ->
                            empty

                        Just smth ->
                            smth.friendDict

                friendDict =
                    Dict.union
                        (fromList
                            (List.map (\u -> ( u.userId, friendAndRelationsFromUser u ))
                                friends
                            )
                        )
                        oldFriendDict

                newUser =
                    case oldUser of
                        Nothing ->
                            oldUser

                        Just smth ->
                            Just { smth | friendDict = friendDict }

                newModel =
                    updateModelWithUser model newUser
            in
            ( newModel, Cmd.none )

        GotBalanceList balances ->
            let
                oldUser =
                    model.user

                oldBalanceDict =
                    case oldUser of
                        Nothing ->
                            Dict.empty

                        Just smth ->
                            smth.balanceDict

                balanceDict =
                    Dict.union (fromList (List.map (\b -> ( b.id, b )) balances)) oldBalanceDict

                newUser =
                    case oldUser of
                        Nothing ->
                            oldUser

                        Just smth ->
                            Just { smth | balanceDict = balanceDict }

                newModel =
                    updateModelWithUser model newUser
            in
            ( newModel
            , fetchManyTrans (List.map (\t -> t.transId) newModel.session.balanceModel.ledger.lastTrans)
            )

        GotTransList transDict ->
            let
                oldUser =
                    model.user

                session =
                    model.session

                newUser =
                    case oldUser of
                        Nothing ->
                            oldUser

                        Just smth ->
                            Just { smth | transDict = transDict }

                balanceModel =
                    session.balanceModel

                newBalanceModel =
                    Balance.updateModelWithTransDict balanceModel transDict

                newSession =
                    { session | balanceModel = newBalanceModel }

                newPage =
                    case model.pageModel of
                        Balance smth ->
                            Balance newBalanceModel

                        _ ->
                            model.pageModel
            in
            ( { model | pageModel = newPage, user = newUser, session = newSession }, Cmd.none )

        GotRequestList requestListKind requests ->
            let
                oldSession =
                    model.session

                havePendingRequests =
                    case requestListKind of
                        Request.PendingRequests ->
                            Request.checkPendingRequests requests

                        _ ->
                            model.havePendingRequests

                newRequestModel =
                    Request.updateWithRequests requestListKind model.session.requestModel requests

                newPage =
                    case model.pageModel of
                        Request smth ->
                            Request newRequestModel

                        _ ->
                            model.pageModel

                newSession =
                    { oldSession | requestModel = newRequestModel }
            in
            ( { model | pageModel = newPage, session = newSession, havePendingRequests = havePendingRequests }, Cmd.none )

        GotProfileMsg signOnMsg ->
            let
                ( newProfileModel, signOnCmd ) =
                    case model.pageModel of
                        Profile signOnModel ->
                            Profile.update signOnMsg signOnModel

                        _ ->
                            Profile.update signOnMsg (Profile.initWithUser model.user)

                ( pageModel, subCmd ) =
                    case model.pageModel of
                        Send sm ->
                            ( Send sm, Cmd.none )

                        SignOn sm ->
                            ( SignOn sm, Cmd.none )

                        Collect sm ->
                            ( Collect sm, Cmd.none )

                        Balance sm ->
                            ( Balance sm, Cmd.none )

                        Friends sm ->
                            ( Friends sm, Cmd.none )

                        Request sm ->
                            ( Request sm, Cmd.none )

                        Profile sm ->
                            ( Profile newProfileModel, signOnCmd )

                session =
                    model.session

                newUser =
                    case model.user of
                        Nothing ->
                            Nothing

                        Just smt ->
                            updateUserWithPrefs smt newProfileModel.newUserProfile.lang newProfileModel.newUserProfile.fiat |> Just

                newSession =
                    { session
                        | profileModel = newProfileModel
                        , sendModel = Send.reset session.sendModel
                        , requestModel = Request.reset session.requestModel
                    }
            in
            ( { model | user = newUser, pageModel = pageModel, session = newSession }, Cmd.map GotProfileMsg subCmd )

        GotSignOnMsg signOnMsg ->
            let
                ( newSignOnModel, signOnCmd ) =
                    case model.pageModel of
                        SignOn signOnModel ->
                            SignOn.update signOnMsg signOnModel

                        _ ->
                            SignOn.update signOnMsg (SignOn.init model.navLang model.collectUrl)

                ( pageModel, subCmd ) =
                    case model.pageModel of
                        Send sm ->
                            ( Send sm, Cmd.none )

                        Profile sm ->
                            ( Profile sm, Cmd.none )

                        Collect sm ->
                            ( Collect sm, Cmd.none )

                        Balance sm ->
                            ( Balance sm, Cmd.none )

                        Friends sm ->
                            ( Friends sm, Cmd.none )

                        Request sm ->
                            ( Request sm, Cmd.none )

                        SignOn sm ->
                            ( SignOn newSignOnModel, signOnCmd )
            in
            ( { model | pageModel = pageModel }, Cmd.map GotSignOnMsg subCmd )

        GotSendMsg sendMsg ->
            let
                session =
                    model.session

                ( subModel, subCmd ) =
                    Tuple.mapSecond (Cmd.map GotSendMsg)
                        (case model.pageModel of
                            Send sendModel ->
                                Send.update sendMsg session.sendModel

                            _ ->
                                ( session.sendModel, Cmd.none )
                        )

                newSession =
                    { session | sendModel = subModel }

                pageModel =
                    case model.pageModel of
                        Send smt ->
                            Send subModel

                        _ ->
                            model.pageModel
            in
            ( { model | pageModel = pageModel, session = newSession }, subCmd )

        GotCollectMsg collectMsg ->
            let
                ( subModel, subCmd ) =
                    Tuple.mapSecond (Cmd.map GotCollectMsg)
                        (case model.pageModel of
                            Collect collectModel ->
                                Collect.update collectMsg collectModel

                            _ ->
                                ( Collect.init Collect.Payment Nothing, Cmd.none )
                        )
            in
            ( { model | pageModel = Collect subModel }, subCmd )

        GotBalanceMsg balanceMsg ->
            let
                session =
                    model.session

                ( subModel, subCmd ) =
                    Tuple.mapSecond (Cmd.map GotBalanceMsg)
                        (case model.pageModel of
                            Balance balanceModel ->
                                Balance.update balanceMsg session.balanceModel

                            _ ->
                                ( session.balanceModel, Cmd.none )
                        )

                newSession =
                    { session | balanceModel = subModel }

                pageModel =
                    case model.pageModel of
                        Balance smt ->
                            Balance subModel

                        _ ->
                            model.pageModel
            in
            ( { model | pageModel = pageModel, session = newSession }, subCmd )

        GotFriendsMsg friendsMsg ->
            let
                session =
                    model.session

                ( subModel, subCmd ) =
                    Tuple.mapSecond (Cmd.map GotFriendsMsg)
                        (case model.pageModel of
                            Friends friendsModel ->
                                Friends.update friendsMsg session.friendsModel

                            _ ->
                                ( session.friendsModel, Cmd.none )
                        )

                newSession =
                    { session | friendsModel = subModel }

                pageModel =
                    case model.pageModel of
                        Friends smt ->
                            Friends subModel

                        _ ->
                            model.pageModel
            in
            ( { model | pageModel = pageModel, session = newSession }, subCmd )

        GotRequestMsg requestMsg ->
            let
                session =
                    model.session

                ( subModel, subCmd ) =
                    Tuple.mapSecond (Cmd.map GotRequestMsg)
                        (case model.pageModel of
                            Request requestModel ->
                                Request.update requestMsg session.requestModel

                            _ ->
                                ( session.requestModel, Cmd.none )
                        )

                newSession =
                    { session | requestModel = subModel }

                pageModel =
                    case model.pageModel of
                        Request smt ->
                            Request subModel

                        _ ->
                            model.pageModel
            in
            ( { model | pageModel = pageModel, session = newSession }, subCmd )


updateModelWithUser : Model -> Maybe User -> Model
updateModelWithUser model newUser =
    case newUser of
        Nothing ->
            model

        Just usrWithoutRates ->
            let
                session =
                    model.session

                usr =
                    updateUserWithRates usrWithoutRates (model.user |> Maybe.map .pref |> Maybe.andThen .rates)

                userId =
                    usr.userId

                friendsModel =
                    session.friendsModel

                newFriendsModel =
                    { friendsModel
                        | user = Just usr
                        , friends = usr.friends
                        , friendDict = usr.friendDict
                    }

                balanceModel =
                    session.balanceModel

                newBalanceModel =
                    Balance.updateModelWithLedgerAndUser balanceModel (get userId usr.balanceDict) usr

                newSendModel =
                    Send.updateWithUser session.sendModel usr

                newRequestModel =
                    Request.updateWithUser session.requestModel usr

                newSession =
                    { session
                        | sendModel = newSendModel
                        , friendsModel = newFriendsModel
                        , balanceModel = newBalanceModel
                        , requestModel = newRequestModel
                    }

                newPage =
                    case model.pageModel of
                        Send smth ->
                            Send newSendModel

                        Friends smth ->
                            Friends newFriendsModel

                        Balance smth ->
                            Balance newBalanceModel

                        Request smth ->
                            Request newRequestModel

                        _ ->
                            model.pageModel
            in
            { model
                | user = Just usr
                , pageModel = newPage
                , session = newSession
            }


updateModelWithRates : Model -> Maybe Rates -> Model
updateModelWithRates model rates =
    { model
        | user = model.user |> Maybe.map (\x -> updateUserWithRates x rates)
    }


view : Model -> Document Msg
view model =
    let
        pref =
            model.user |> Maybe.map .pref

        goldPrinter =
            pref |> Pref.getGoldPrinter

        converter =
            pref |> Pref.getConverter

        itextLang =
            pref |> Pref.getTranslator

        screen =
            case model.pageModel of
                Profile _ ->
                    ProfileScreen

                Send _ ->
                    SendScreen

                SignOn _ ->
                    SignOnScreen

                Collect _ ->
                    CollectScreen

                Balance _ ->
                    BalanceScreen

                Friends _ ->
                    FriendsScreen

                Request _ ->
                    RequestScreen

        bodyContent =
            case model.pageModel of
                Profile signOnModel ->
                    Element.map GotProfileMsg (Profile.view itextLang signOnModel)

                Send sendModel ->
                    Element.map GotSendMsg (Send.view goldPrinter converter itextLang sendModel)

                SignOn signOnModel ->
                    Element.map GotSignOnMsg (SignOn.view signOnModel)

                Collect collectModel ->
                    Element.map GotCollectMsg (Collect.view itextLang collectModel)

                Balance balanceModel ->
                    Element.map GotBalanceMsg (Balance.view goldPrinter itextLang balanceModel)

                Friends subModel ->
                    Element.map GotFriendsMsg (Friends.view goldPrinter itextLang subModel)

                Request subModel ->
                    Element.map GotRequestMsg (Request.view goldPrinter converter itextLang subModel)

        navButtons =
            case model.pageModel of
                SignOn signOnModel ->
                    None

                Send sendModel ->
                    Back (viewBackButton itextLang)

                Request sendModel ->
                    Back (viewBackButton itextLang)

                _ ->
                    All (viewNavbar itextLang screen model.havePendingRequests) (viewSendButton itextLang screen)
    in
    { title = "Back2Gold"
    , body =
        [ mergeNavbarWithContent navButtons
            (if debugMode then
                withDebugInfo goldPrinter model bodyContent

             else
                bodyContent
            )
        ]
    }


debugMode : Bool
debugMode =
    False


withDebugInfo : (Int -> GoldAndFiat) -> Model -> Element msg -> Element msg
withDebugInfo goldPrinter model content =
    column [ width fill ]
        [ goldFiatToParagraph <| goldPrinter <| 1000
        , el []
            (Element.text
                (if model.havePendingRequests then
                    "Sí"

                 else
                    "no"
                )
            )
        , content
        ]


type NavigationButtons
    = None
    | All (Element Msg) (Element Msg)
    | Back (Element Msg)


mergeNavbarWithContent : NavigationButtons -> Element Msg -> Html Msg
mergeNavbarWithContent navButtons content =
    Element.layout [ Background.color Style.cBG1 ]
        (case navButtons of
            All navbar sendButton ->
                column [ width fill ]
                    [ el
                        [ width fill
                        , inFront
                            (el
                                [ paddingXY 5 6
                                , alpha 0.8
                                , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 10, bottomRight = 0 }
                                , fontSizeOfDefault 1
                                , alignRight
                                , Font.color Style.cText1
                                , Background.color Style.cGold
                                , fontSizeOfDefault 0.8
                                ]
                                (Element.text " BETA")
                            )
                        ]
                        content
                    , el [ centerX, paddingXY 0 80 ] (Element.text "")
                    , sendButton
                    , navbar
                    ]

            Back backButton ->
                column [ width fill ]
                    [ el
                        [ width fill
                        , inFront
                            (el
                                [ paddingXY 5 6
                                , alpha 0.8
                                , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 10, bottomRight = 0 }
                                , fontSizeOfDefault 1
                                , alignRight
                                , Font.color Style.cText1
                                , Background.color Style.cGold
                                , fontSizeOfDefault 0.8
                                ]
                                (Element.text " BETA")
                            )
                        ]
                        content
                    , backButton
                    ]

            None ->
                column [ width fill ] [ content ]
        )


type Icon
    = ProfileIcon
    | BalanceIcon
    | FriendsIcon


viewSendButton itext screen =
    let
        requestRow =
            row
                [ paddingXY 15 5
                , Background.color Style.cDarkGold
                , Border.rounded 30
                , spacing 10
                , onClick (GoTo RequestScreen)
                , alignRight
                ]
                [ image
                    [ height (px 25) ]
                    { src = Asset.src Asset.askIcon, description = "recieve" }
                , IrAPedir |> itext |> Element.text |> el [ Font.color Style.cText3, Font.bold ]
                ]

        sendRow =
            row
                [ paddingXY 15 5
                , Background.color Style.cButton
                , Border.rounded 30
                , spacing 10
                , onClick (GoTo SendScreen)
                , alignRight
                ]
                [ image
                    [ height (px 25) ]
                    { src = Asset.src Asset.sendActiveIcon, description = "send" }
                , IrAEnviar |> itext |> Element.text |> el [ Font.color Style.cText3, Font.bold ]
                ]
    in
    column
        [ htmlAttribute (style "position" "fixed")
        , htmlAttribute (style "bottom" "0")
        , htmlAttribute (style "overflow" "hidden")
        , spacing 12
        , alignRight
        , moveUp 70
        , moveLeft 5
        , fontSizeOfDefault 1
        ]
        (case screen of
            SendScreen ->
                [ none ]

            RequestScreen ->
                [ none ]

            _ ->
                [ requestRow
                , sendRow
                ]
        )


viewNavbar : (Texto -> String) -> Screen -> Bool -> Element Msg
viewNavbar itext screen havePendingRequests =
    row
        [ Style.navBarGradient
        , spaceEvenly
        , width fill
        , height (px 55)
        , htmlAttribute (style "position" "fixed")
        , htmlAttribute (style "bottom" "0")
        , htmlAttribute (style "overflow" "hidden")
        , paddingXY 15 0
        ]
        [ el [] (viewIcon itext screen ProfileIcon)
        , el [] (viewIcon itext screen FriendsIcon)
        , el [] (viewIcon itext screen BalanceIcon)
        ]


viewBackButton : (Texto -> String) -> Element Msg
viewBackButton itext =
    row
        [ paddingXY 25 0
        , Background.color Style.cDarkGold
        , onClick (GoTo BalanceScreen)
        , alignRight
        , height (px 40)
        , htmlAttribute (style "position" "fixed")
        , htmlAttribute (style "bottom" "0")
        , htmlAttribute (style "overflow" "hidden")
        ]
        [ "❮" |> Element.text |> el [ Font.color Style.cText3, Font.bold, fontSizeOfDefault 1.3 ]
        ]


type Selected
    = Yes
    | No


decoratedIcon : String -> String -> Selected -> Screen -> Element Msg
decoratedIcon text src selected screen =
    let
        imgAttrs =
            case selected of
                Yes ->
                    [ height (px 25), centerX ]

                No ->
                    [ height (px 23), centerX, alpha 0.5 ]

        textAttrs =
            [ Font.color Style.cText1, fontSizeOfDefault 1.1 ]
                |> (::)
                    (case selected of
                        Yes ->
                            Font.bold

                        No ->
                            alpha 0.7
                    )
    in
    column
        [ onClick (GoTo screen) ]
        [ image imgAttrs { src = src, description = text }
        , el textAttrs (Element.text text)
        ]


capitalize : String -> String
capitalize input =
    (left 1 <| input) ++ (dropLeft 1 <| toLower <| input)


viewIcon : (Texto -> String) -> Screen -> Icon -> Element Msg
viewIcon itext screen iconType =
    --use uncons to capitalize
    case iconType of
        BalanceIcon ->
            let
                selected =
                    case screen of
                        BalanceScreen ->
                            Yes

                        _ ->
                            No
            in
            decoratedIcon (capitalize <| itext I18n.Balance) (Asset.src Asset.balanceIcon) selected BalanceScreen

        ProfileIcon ->
            let
                selected =
                    case screen of
                        ProfileScreen ->
                            Yes

                        _ ->
                            No
            in
            decoratedIcon (capitalize <| itext I18n.Perfil) (Asset.src Asset.profileIcon) selected ProfileScreen

        FriendsIcon ->
            let
                selected =
                    case screen of
                        FriendsScreen ->
                            Yes

                        _ ->
                            No
            in
            decoratedIcon (capitalize <| itext I18n.Contactos) (Asset.src Asset.friendsIcon) selected FriendsScreen
