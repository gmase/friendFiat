module Date exposing (timestampToDate)

import Time exposing (Month(..), Zone, millisToPosix, toDay, toHour, toMinute, toMonth, toSecond)


timestampToDate : Zone -> Int -> String
timestampToDate zone t =
    millisToPosix t |> timeToString zone


getMonth : Zone -> Time.Posix -> String
getMonth zone time =
    let
        month =
            toMonth zone time
    in
    case month of
        Jan ->
            "Ene"

        Feb ->
            "Feb"

        Mar ->
            "Mar"

        Apr ->
            "Abr"

        May ->
            "May"

        Jun ->
            "Jun"

        Jul ->
            "Jul"

        Aug ->
            "Ago"

        Sep ->
            "Sep"

        Oct ->
            "Oct"

        Nov ->
            "Nov"

        Dec ->
            "Dic"


timeToString : Time.Zone -> Time.Posix -> String
timeToString zone time =
    String.fromInt (toDay zone time)
        ++ "-"
        ++ getMonth zone time
        ++ " "
        ++ String.fromInt (toHour zone time)
        ++ ":"
        ++ (formatMinSec <| String.fromInt (toMinute zone time))
        ++ ":"
        ++ (formatMinSec <| String.fromInt (toSecond zone time))


formatMinSec : String -> String
formatMinSec minutes =
    String.repeat (2 - String.length minutes) "0" ++ minutes
