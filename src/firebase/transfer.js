import { ref, serverTimestamp, push } from 'firebase/database';

// sendMoney
const sendMoney = (db, app, transfer) => {
    var transferId = push(ref(db, '/transfers'), transfer).key;
    app.ports.receiveTransferId.send(transferId);
};
export const formatAmount = amount => {
    if (!amount) {
        return null;
    }
    let amountObj = {};
    for (let i = 0; i < amount.length; i++) {
        let key = amount[i].fiat;
        let value = amount[i].qty;
        amountObj[key] = value;
    }
    return amountObj;
};
export const subscribeToSendMoney = params => {
    params.elmApp.ports.sendMoney.subscribe(transfer => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            transfer.createdAt = serverTimestamp();
            transfer.from = user.uid;
            transfer.amount = formatAmount(transfer.amount);
            sendMoney(params.db, params.elmApp, transfer);
        }
    });
};

// invites
const sendInvite = (db, app, transfer) => {
    var transferId = push(ref(db, '/transfers'), transfer).key;
    app.ports.receiveInviteId.send(transferId);
};

export const subscribeToSendInvite = params => {
    params.elmApp.ports.sendInvite.subscribe(transfer => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            transfer.createdAt = serverTimestamp();
            transfer.from = user.uid;
            sendInvite(params.db, params.elmApp, transfer);
        }
    });
};

// claim
const claim = (db, app, claimData, userId) => {
    claimData.claimer = userId;
    claimData.createdAt = serverTimestamp();
    push(ref(db, '/claims'), claimData);
};

export const subscribeToClaim = params => {
    params.elmApp.ports.claim.subscribe(claimData => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            claim(params.db, params.elmApp, claimData, user.uid);
        }
    });
};

// Copy to clipboard
export const subscribeToCopyToClipboard = params => {
    params.elmApp.ports.copyToClipboard.subscribe(() => {
        var range = document.createRange();
        var copyText = document.getElementById('copy');
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/
        document.execCommand('copy');
    });
};
