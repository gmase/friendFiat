import { ref, onValue } from 'firebase/database';

export const doOnValueOverArray = (db, baseRef, refs, formatData) => {
    return refs.map(ref => onValueAsPromise(db, baseRef, ref, formatData));
};

const onValueAsPromise = (db, baseRef, currentRef, formatData) => {
    let callOnValue = resolve =>
        onValue(
            ref(db, baseRef + currentRef),
            snapshot => {
                resolve(
                    snapshot.exists()
                        ? formatData(snapshot.val(), currentRef)
                        : null
                );
            },
            { onlyOnce: true }
        );
    return new Promise((resolve, reject) => {
        callOnValue(resolve);
    });
};

// DO NOT USE - Ugly alternative as it is sequential
export const doToArrayOfRefs = (
    db,
    baseRef,
    currentRef,
    refs,
    values,
    cb,
    formatData
) => {
    onValue(
        ref(db, baseRef + currentRef),
        snapshot => {
            if (snapshot.exists()) {
                values.push(formatData(snapshot.val(), currentRef));
            }
            if (refs.length === 0) {
                cb(values);
                return;
            }
            var newRef = refs.pop();
            doToArrayOfRefs(db, baseRef, newRef, refs, values, cb, formatData);
        },
        { onlyOnce: true }
    );
};
