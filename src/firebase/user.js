import { onAuthStateChanged } from 'firebase/auth';
import { ref, set, onValue, child } from 'firebase/database';
import { getToken } from 'firebase/messaging';
import { firebaseConfig as config, vapidKey } from './config.js';
import { doOnValueOverArray } from './utils.js';

const getDefaultLanguage = () => {
    let lang = navigator.language || navigator.userLanguage || '_noLanguage';
    lang = lang.substring(0, 2);
    switch (lang) {
        case 'es':
            return 'Es';
        case 'en':
            return 'En';
        case 'fr':
            return 'Fr';
        default:
            return 'En';
    }
};

const defaultProfile = {
    name: 'new user',
    language: getDefaultLanguage(),
    fiat: 'EUR',
};
// fetchUser
const formatUser = (input, userId) => {
    input.userId = userId;
    if (input.friends) {
        let processedFriends = Object.values(input.friends);
        let keys = Object.keys(input.friends);
        for (let i = 0; i < processedFriends.length; i++) {
            processedFriends[i].id = keys[i];
        }
        input.friends = processedFriends;
    }
    return input;
};

const fetchFriends = (db, app, users) => {
    let uniqueUsers = [...new Set(users)];
    Promise.all(
        doOnValueOverArray(db, '/users/', uniqueUsers, formatUser)
    ).then(values =>
        app.ports.receiveFriends.send(
            values.filter(friend => friend && friend.name)
        )
    );
};

export const subscribeToFetchFriends = params => {
    params.elmApp.ports.fetchFriends.subscribe(users => {
        fetchFriends(params.db, params.elmApp, users);
    });
};

const checkAndFetchUser = (db, auth, app, userId) => {
    return onValue(ref(db, '/users/' + userId), snapshot => {
        let input = defaultProfile;
        if (snapshot.exists()) {
            input = snapshot.val();
            input = formatUser(input, userId);
        } else {
            createUser(db, auth, userId, input);
        }
        app.ports.receiveUser.send(input);
    });
};

const fetchRates = (db, app) => {
    return onValue(ref(db, '/metals/rates'), snapshot => {
        let input = {};
        if (snapshot.exists()) {
            input = snapshot.val();
        }
        app.ports.receiveRates.send(input);
    });
};

const createUser = (db, auth, uid, profile) => {
    let email = auth.currentUser.email.split('@')[0];
    profile.name = profile.name === defaultProfile.name ? email : profile.name;
    set(ref(db, '/users/' + uid), profile);
};

const pushDeviceToken = (db, uid, token) => {
    set(ref(db, '/tokens/' + uid + '/notificationTokens/' + token), 1);
};

// Get registration token. Initially this makes a network call, once retrieved
// subsequent calls to getToken will return from cache.
const tokenStuff = (params, uid) => {
    if (!params.messaging) {
        console.log('No messaging available');
        return;
    }
    let messaging = params.messaging;
    getToken(messaging, { vapidKey: vapidKey })
        .then(currentToken => {
            if (currentToken) {
                pushDeviceToken(params.db, uid, currentToken);
                return;
            }
            if (
                Notification.permission !== 'granted' &&
                Notification.permission !== 'denied'
            ) {
                Notification.requestPermission().then(function(permission) {
                    // If the user accepts, let's create a notification
                    if (permission === 'granted') {
                        getToken(messaging, { vapidKey: vapidKey }).then(
                            newToken => {
                                if (newToken) {
                                    pushDeviceToken(params.db, uid, newToken);
                                }
                            }
                        );
                    }
                });
            }
            // Show permission request UI
            console.log(
                'No registration token available. Request permission to generate one.'
            );
        })
        .catch(err => {
            console.log('An error occurred while retrieving token. ', err);
        });
};

export const subscribeToUserChange = params => {
    onAuthStateChanged(params.auth, user => {
        if (user) {
            checkAndFetchUser(params.db, params.auth, params.elmApp, user.uid);
            fetchRates(params.db, params.elmApp);
            tokenStuff(params, user.uid);
        } else {
            params.elmApp.ports.noUser.send(true);
        }
    });
};

// sendNewProfile

const updateProfile = (db, profile, uid, email) => {
    set(
        child(ref(db, '/users/' + uid), 'name'),
        profile.name || email.split('@')[0]
    );
    set(child(ref(db, '/users/' + uid), 'language'), profile.language);
    set(child(ref(db, '/users/' + uid), 'fiat'), profile.fiat);
};

export const subscribeToSendNewProfile = params => {
    params.elmApp.ports.sendNewProfile.subscribe(profile => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            updateProfile(params.db, profile, user.uid, user.email);
            tokenStuff(params, user.uid);
        }
    });
};

// fetchBalance

const formatBalance = (balance, userId) => {
    balance.id = userId;
    if (balance.have) {
        let haveValues = Object.values(balance.have);
        let haveKeys = Object.keys(balance.have);
        balance.have = [];
        for (let i = 0; i < haveValues.length; i++) {
            balance.have.push({ fiat: haveKeys[i], qty: haveValues[i] });
        }
    }
    if (balance.lastTrans) {
        balance.lastTrans = Object.values(balance.lastTrans);
    }
    return balance;
};
const fetchBalance = (db, app, userId) => {
    return onValue(ref(db, '/balances/' + userId), snapshot => {
        if (snapshot.exists()) {
            let input = formatBalance(snapshot.val(), userId);
            app.ports.receiveBalance.send([input]);
        }
    });
};
export const subscribeToFetchBalance = params => {
    params.elmApp.ports.fetchBalance.subscribe(profile => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            fetchBalance(params.db, params.elmApp, user.uid);
        }
    });
};

// fetchManyBalances
const fetchManyBalances = (db, app, users) => {
    let uniqueUsers = [...new Set(users)];
    Promise.all(
        doOnValueOverArray(db, '/balances/', uniqueUsers, formatBalance)
    ).then(values => app.ports.receiveManyBalances.send(values));
};
export const subscribeToFetchManyBalances = params => {
    params.elmApp.ports.fetchManyBalances.subscribe(friends => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            fetchManyBalances(params.db, params.elmApp, friends);
        }
    });
};

// TRANS

const formatTrans = (trans, transId) => {
    trans.id = transId;
    let amountValues = Object.values(trans.amount);
    let amountKeys = Object.keys(trans.amount);
    let processedAmount = [];
    for (let i = 0; i < amountValues.length; i++) {
        processedAmount.push({ fiat: amountKeys[i], qty: amountValues[i] });
    }
    trans.amount = processedAmount;
    return trans;
};

const fetchManyTrans = (db, app, transList) => {
    Promise.all(
        doOnValueOverArray(db, '/transfers/', transList, formatTrans)
    ).then(values => app.ports.receiveManyTrans.send(values));
};
export const subscribeToFetchManyTrans = params => {
    params.elmApp.ports.fetchManyTrans.subscribe(transList => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            fetchManyTrans(params.db, params.elmApp, transList);
        }
    });
};
