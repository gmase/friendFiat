import {
    ref,
    set,
    onValue,
    child,
    push,
    query,
    orderByChild,
    equalTo,
    serverTimestamp,
} from 'firebase/database';
import { formatAmount } from './transfer.js';

// sendRequest
const sendRequest = (db, request) => {
    push(ref(db, '/requests'), request);
};

export const subscribeToSendRequest = params => {
    params.elmApp.ports.sendRequest.subscribe(request => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            request.createdAt = serverTimestamp();
            request.from = user.uid;
            request.amountFrom.moneyPersons = formatAmount(
                request.amountFrom.moneyPersons
            );
            request.amountTo.moneyPersons = formatAmount(
                request.amountTo.moneyPersons
            );
            sendRequest(params.db, request);
        }
    });
};

const processAmountWithTotalFromDB = raw => {
    let processedAmount = [];
    if (!raw) {
        return processedAmount;
    }
    let amountValues = Object.values(raw);
    let amountKeys = Object.keys(raw);
    for (let i = 0; i < amountValues.length; i++) {
        processedAmount.push({ fiat: amountKeys[i], qty: amountValues[i] });
    }
    return processedAmount;
};

// read requests

const formatRequests = requests => {
    let reqs = [];
    if (!requests) {
        return reqs;
    }
    let values = Object.values(requests);
    let keys = Object.keys(requests);
    for (let i = 0; i < values.length; i++) {
        reqs.push(formatRequest(values[i], keys[i]));
    }
    return reqs;
};
const formatRequest = (request, requestId) => {
    request.id = requestId;
    request.amountFrom.moneyPersons = processAmountWithTotalFromDB(
        request.amountFrom.moneyPersons
    );
    request.amountTo.moneyPersons = processAmountWithTotalFromDB(
        request.amountTo.moneyPersons
    );
    return request;
};

const fetchManyRequests = (db, auth, appPort, toFrom) => {
    var user = auth.currentUser;
    if (user && user.uid) {
        return onValue(
            query(
                ref(db, '/requests'),
                orderByChild(toFrom),
                equalTo(user.uid)
            ),
            snapshot => {
                let requests = formatRequests(snapshot.val());
                appPort.send(requests);
            }
        );
    }
};

export const subscribeToFetchManyRequest = params => {
    params.elmApp.ports.fetchManyRequests.subscribe(temp => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            fetchManyRequests(
                params.db,
                params.auth,
                params.elmApp.ports.receiveManyRequests,
                'to'
            );
        }
    });
};

export const subscribeToFetchMyPendingRequest = params => {
    params.elmApp.ports.fetchMyPendingRequests.subscribe(temp => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            fetchManyRequests(
                params.db,
                params.auth,
                params.elmApp.ports.receiveMyPendingRequests,
                'from'
            );
        }
    });
};

// Respond to request

const respondToRequest = (db, app, response) => {
    set(child(ref(db, '/requests/' + response.id), 'status'), response.status);
};

export const subscribeToRespondToRequest = params => {
    params.elmApp.ports.respondToRequest.subscribe(response => {
        var user = params.auth.currentUser;
        if (user && user.uid) {
            respondToRequest(params.db, params.elmApp, response);
        }
    });
};
