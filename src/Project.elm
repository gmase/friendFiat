module Project exposing (News, NewsEntry, ProjectNews, newsEntryFromProjectNews)

type alias ProjectNews =
    { title : String
    , news : List News
    }


type alias News =
    { summary : String
    , text : String
    }


type alias NewsEntry =
    { title : String
    , summary : String
    , author : String
    }


newsEntryFromProjectNews : ProjectNews -> NewsEntry
newsEntryFromProjectNews projectNews =
    NewsEntry
        projectNews.title
        (Maybe.withDefault "no news" <| Maybe.map .summary <| List.head projectNews.news)
        "by Guillermo Martos"
