module Money exposing (Money, MoneyPerson, moneyPersonToRow, sumOfMoneyPersons)

import Element exposing (el, fill, paragraph, row, width)
import Gold exposing (goldFiatToFiatElement)


type alias Money =
    Int


type alias MoneyPerson =
    { fiat : String
    , qty : Money
    }


sumOfMoneyPersons : List MoneyPerson -> MoneyPerson
sumOfMoneyPersons mp =
    let
        fiat =
            mp |> List.head |> Maybe.withDefault { fiat = "...", qty = 0 } |> .fiat

        qty =
            mp |> List.map .qty |> List.sum
    in
    MoneyPerson fiat qty



-- MONEY_PERSON_TO_ROW


defaultNoName =
    "????"


personToFiatName : String -> String
personToFiatName name =
    let
        words =
            String.words name
    in
    (if List.length words < 2 then
        name |> String.slice 0 4

     else
        case words of
            x :: xs ->
                List.head xs
                    |> Maybe.withDefault " "
                    |> String.slice 0 1
                    |> (++) (String.slice 0 3 x)

            [] ->
                defaultNoName
    )
        |> String.toUpper


moneyPersonToRowInternal formatFn printGold nameSearcher moneyPerson =
    let
        moneyName =
            nameSearcher moneyPerson.fiat |> Maybe.withDefault defaultNoName |> personToFiatName

        goldFiat =
            printGold moneyPerson.qty
    in
    row [ width fill ]
        [ paragraph []
            [ goldFiat
                |> .gold
                |> Element.text
                |> el []
            , el [] (Element.text (" " ++ moneyName ++ "ᨀ"))
            ]
        ]


moneyPersonToRow =
    moneyPersonToRowInternal (goldFiatToFiatElement [])



-- ᎕ ⌭ ⼂ᐥ ᜶ ⫽ ᨀ ީ ާ
