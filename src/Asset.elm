module Asset exposing (logo, balanceIcon, friendsIcon, inIcon, loading, outIcon, profileIcon, requestIcon, requestPendingIcon, sendActiveIcon, src, askIcon)


type Icon
    = Icon String



-- ICONS

logo : Icon
logo =
    icon "logoNoBackground.png"


loading : Icon
loading =
    icon "loading.svg"


sendActiveIcon : Icon
sendActiveIcon =
    icon "moneyWhite.svg"

askIcon : Icon
askIcon =
    icon "askIcon.svg"


balanceIcon : Icon
balanceIcon =
    icon "balance.svg"


profileIcon : Icon
profileIcon =
    icon "profile.svg"


friendsIcon : Icon
friendsIcon =
    icon "friends.svg"


requestIcon : Icon
requestIcon =
    icon "request.svg"


requestPendingIcon : Icon
requestPendingIcon =
    icon "requestPending.svg"


inIcon : Icon
inIcon =
    icon "in.svg"


outIcon : Icon
outIcon =
    icon "out.svg"


icon : String -> Icon
icon filename =
    Icon ("assets/icons/" ++ filename)



-- USING ICONS


src : Icon -> String
src (Icon url) =
    url
