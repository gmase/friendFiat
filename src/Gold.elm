module Gold exposing (Fiat, GoldAndFiat, Rates, availableFiats, convert, decodeLocale, decodeRates, defaultFiat, eur, fiatDecoder, fiatToText, gbp, getFiatFromCode, goldFiatToFiatElement, goldFiatToFiatString, goldFiatToParagraph, goldFiatToString, goldFiatToTitle, maxGold, pen, printGold, stringToGold, usd)

import Element exposing (Attribute, Element, column, el, none, paragraph, spacing)
import FormatNumber exposing (format)
import FormatNumber.Locales exposing (Locale, frenchLocale, fromString)
import Json.Decode as Decode exposing (float, string, succeed)
import Json.Decode.Pipeline exposing (required)
import Style exposing (fiatFormat)



-- RESPONSIBILITY: Currencies and currencies to element/text
-- type alias Gold =
--     Int


stringToGold : String -> Int
stringToGold =
    String.toFloat >> Maybe.withDefault 0 >> round


maxGold : Int
maxGold =
    -- 100Kg
    100000000


type alias Rates =
    { ars : Float
    , chf : Float
    , eur : Float
    , gbp : Float
    , usd : Float
    , pen : Float
    }


type SymbolPosition
    = Before
    | After


type alias Fiat =
    { name : String
    , code : String
    , symbol : String
    , decimals : Int
    , position : SymbolPosition
    }


getFiatFromCode : String -> Fiat
getFiatFromCode code =
    availableFiats
        |> List.filter (\x -> x.code == code)
        |> List.head
        |> Maybe.withDefault defaultFiat


fiatToText : Fiat -> String
fiatToText fiat =
    fiat.symbol ++ " - " ++ fiat.name


ounceToGr =
    31.1034768


mgRate =
    ounceToGr * 1000


zeroGold =
    "0☉"


decodeLocale : Decode.Value -> Locale
decodeLocale json =
    let
        locale =
            case Decode.decodeValue (Decode.field "numberFormat" Decode.string) json of
                Ok value ->
                    fromString value

                Err msg ->
                    frenchLocale

        thousandSeparator =
            if locale.thousandSeparator == "" then
                if locale.decimalSeparator /= "." then
                    "."

                else
                    ","

            else
                locale.thousandSeparator
    in
    { locale | decimals = FormatNumber.Locales.Exact 0, thousandSeparator = thousandSeparator }


formatFloat : Locale -> Float -> String
formatFloat locale =
    format locale


formatInt : Locale -> Int -> String
formatInt locale amount =
    toFloat amount
        |> formatFloat locale


showGoldAmount : Locale -> Int -> String
showGoldAmount locale amount =
    case amount of
        0 ->
            zeroGold

        _ ->
            if abs amount < 100000 then
                amount
                    |> formatInt locale
                    |> (++) "mg☉ "

            else
                (toFloat amount / 1000)
                    |> withPrecision 2
                    |> String.fromFloat
                    |> (++) "g☉ "


goldFiatToFiatString : GoldAndFiat -> String
goldFiatToFiatString goldAndFiat =
    goldAndFiat.preSymbol
        ++ goldAndFiat.fiat
        ++ goldAndFiat.postSymbol


goldFiatToString : GoldAndFiat -> String
goldFiatToString goldAndFiat =
    goldAndFiat.gold ++ " (" ++ goldFiatToFiatString goldAndFiat ++ ") "


type alias GoldAndFiat =
    { gold : String
    , preSymbol : String
    , fiat : String
    , postSymbol : String
    , locale : Locale
    }


convert : Maybe Rates -> Fiat -> (Bool -> Float -> Float)
convert rates fiat toGold amount =
    let
        rate =
            getRate rates fiat
    in
    if toGold then
        amount
            / (rate / mgRate)
            |> round
            |> toFloat

    else
        amount
            * (rate / mgRate)
            |> withPrecision fiat.decimals


goldFiatToFiatElement : List (Attribute msg) -> GoldAndFiat -> Element msg
goldFiatToFiatElement extraAttr goldAndFiat =
    if goldAndFiat.gold == zeroGold then
        none

    else
        goldFiatToFiatString goldAndFiat
            |> Element.text
            |> el (extraAttr ++ fiatFormat)


goldFiatToParagraph : GoldAndFiat -> Element msg
goldFiatToParagraph goldAndFiat =
    paragraph []
        [ goldAndFiat.gold
            |> Element.text
            |> el []
        , goldFiatToFiatElement [] goldAndFiat
        ]


goldFiatToTitle : GoldAndFiat -> Element msg
goldFiatToTitle goldAndFiat =
    column [ spacing 3 ]
        [ goldFiatToFiatElement [] goldAndFiat
        , goldAndFiat.gold
            |> Element.text
            |> el []
        ]


printGold : Maybe Rates -> Locale -> Fiat -> (Int -> GoldAndFiat)
printGold rates locale fiat goldAmount =
    let
        fiatAmount =
            convert rates fiat False (toFloat goldAmount)

        symbolElement =
            fiat.symbol

        preSymbol =
            case fiat.position of
                After ->
                    ""

                Before ->
                    symbolElement

        postSymbol =
            case fiat.position of
                After ->
                    symbolElement

                Before ->
                    ""
    in
    GoldAndFiat (showGoldAmount locale goldAmount) preSymbol (String.fromFloat fiatAmount) postSymbol locale


withPrecision : Int -> Float -> Float
withPrecision decimals init =
    let
        mult =
            toFloat (10 ^ decimals)
    in
    toFloat (round (init * mult)) / mult


decodeRates : Decode.Value -> Maybe Rates
decodeRates json =
    case Decode.decodeValue ratesDecoder json of
        Ok value ->
            Just value

        Err _ ->
            Nothing


ratesDecoder : Decode.Decoder Rates
ratesDecoder =
    succeed Rates
        |> required "ARS" float
        |> required "CHF" float
        |> required "EUR" float
        |> required "GBP" float
        |> required "USD" float
        |> required "PEN" float


fiatDecoder : Decode.Decoder Fiat
fiatDecoder =
    string
        |> Decode.andThen
            (\str ->
                case str of
                    "EUR" ->
                        succeed eur

                    "USD" ->
                        succeed usd

                    "GBP" ->
                        succeed gbp

                    "PEN" ->
                        succeed pen

                    "ARS" ->
                        succeed ars

                    _ ->
                        succeed defaultFiat
            )


getRate : Maybe Rates -> Fiat -> Float
getRate rates fiat =
    case rates of
        Nothing ->
            1

        Just smth ->
            case fiat.code of
                "ARS" ->
                    smth.ars

                "CHF" ->
                    smth.chf

                "EUR" ->
                    smth.eur

                "GBP" ->
                    smth.gbp

                "USD" ->
                    smth.usd

                "PEN" ->
                    smth.pen

                _ ->
                    smth.eur


availableFiats : List Fiat
availableFiats =
    [ eur
    , usd
    , gbp
    , pen
    , ars
    ]


defaultFiat =
    eur


eur : Fiat
eur =
    { name = "Euro"
    , code = "EUR"
    , symbol = "€"
    , decimals = 2
    , position = After
    }


usd : Fiat
usd =
    { name = "US Dollar"
    , code = "USD"
    , symbol = "$"
    , decimals = 2
    , position = Before
    }


gbp : Fiat
gbp =
    { name = "GB Pound"
    , code = "GBP"
    , symbol = "£"
    , decimals = 2
    , position = Before
    }


pen : Fiat
pen =
    { name = "Sol peruano"
    , code = "PEN"
    , symbol = "S/"
    , decimals = 2
    , position = Before
    }


ars : Fiat
ars =
    { name = "Peso argentino"
    , code = "ARS"
    , symbol = "$"
    , decimals = 0
    , position = Before
    }
