module Qr exposing (qrCodeView)

import Html exposing (Html)
import QRCode
import Svg.Attributes as SvgA


qrCodeView : String -> Html msg
qrCodeView message =
    QRCode.fromString message
        |> Result.map
            (QRCode.toSvg
                [ SvgA.width "200px"
                , SvgA.height "200px"
                ]
            )
        |> Result.withDefault (Html.text "Error while encoding to QRCode.")
