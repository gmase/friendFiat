module Pref exposing (Pref, getConverter, getGoldPrinter, getTranslator)

import FormatNumber.Locales exposing (Locale, base)
import Gold exposing (Fiat, GoldAndFiat, Rates, convert, printGold)
import I18n


type alias Pref =
    { lang : I18n.Language
    , fiat : Fiat
    , rates : Maybe Rates
    , locale : Locale
    }


getTranslator : Maybe Pref -> (I18n.Texto -> String)
getTranslator pref =
    case pref of
        Just smt ->
            I18n.itext smt.lang smt.fiat.symbol

        Nothing ->
            \_ -> "Text not found"


getConverter : Maybe Pref -> (Bool -> Float -> Float)
getConverter pref =
    case pref of
        Just smt ->
            convert smt.rates smt.fiat

        Nothing ->
            \_ _ -> 1


getGoldPrinter : Maybe Pref -> (Int -> GoldAndFiat)
getGoldPrinter pref =
    case pref of
        Just smt ->
            printGold smt.rates smt.locale smt.fiat

        Nothing ->
            \_ ->
                { gold = ""
                , preSymbol = ""
                , fiat = ""
                , postSymbol = ""
                , locale = base
                }
