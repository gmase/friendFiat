module User exposing (BalanceDict, Friend, FriendDict, TransDict, User, decodeManyTrans, decodeUser, decodeUsers, friendAndRelationsFromUser, getFriend, getFriendNameFromDict, getInitUser, getUserName, updateUserWithPrefs, updateUserWithRates, userSorter)

import Dict exposing (Dict, empty, fromList, get)
import FormatNumber.Locales exposing (Locale, base)
import Gold exposing (Fiat, Rates, defaultFiat, fiatDecoder)
import I18n exposing (Language(..))
import Json.Decode as Decode exposing (list, string, succeed)
import Json.Decode.Pipeline exposing (hardcoded, optional, required)
import Ledger exposing (Ledger)
import Ports exposing (TransferData, transDecoder)
import Pref exposing (Pref)


type FriendStatus
    = Accept
    | Dont


type alias FriendBalance =
    { owe : Int
    , have : Int
    }


type alias Friend =
    { id : String
    , status : FriendStatus
    , friendBalance : Maybe FriendBalance
    }


getFriend : String -> Friend
getFriend id =
    { id = id
    , status = Accept
    , friendBalance = Nothing
    }


type alias FriendAndRelations =
    { name : String
    , friends : List String
    }



-- Only friends you accept


friendAndRelationsFromUser : User -> FriendAndRelations
friendAndRelationsFromUser user =
    FriendAndRelations user.name (List.map (\f -> f.id) (List.filter (\f -> f.status == Accept) user.friends))


type alias FriendDict =
    Dict String FriendAndRelations


type alias BalanceDict =
    Dict String Ledger


type alias TransDict =
    Dict String TransferData


type alias ImportedUser =
    { userId : String
    , name : String
    , language : Language
    , fiat : Fiat
    , friends : List Friend
    , friendDict : FriendDict
    , balanceDict : BalanceDict
    , transDict : TransDict
    }


type alias User =
    { userId : String
    , name : String
    , friends : List Friend
    , friendDict : FriendDict
    , balanceDict : BalanceDict
    , transDict : TransDict
    , pref : Pref
    }


getInitUser : User
getInitUser =
    { userId = "guest"
    , name = ""
    , friends = []
    , friendDict = empty
    , balanceDict = empty
    , transDict = empty
    , pref =
        { lang = Es
        , fiat = defaultFiat
        , rates = Nothing
        , locale = base
        }
    }


updateUserWithRates : User -> Maybe Rates -> User
updateUserWithRates user rates =
    let
        pref =
            user.pref
    in
    { user | pref = { pref | rates = rates } }


updateUserWithPrefs : User -> Language -> Fiat -> User
updateUserWithPrefs user lang fiat =
    let
        pref =
            user.pref
    in
    { user | pref = { pref | lang = lang, fiat = fiat } }


userFromImported : Locale -> ImportedUser -> User
userFromImported locale imported =
    { userId = imported.userId
    , name = imported.name
    , friends = imported.friends
    , friendDict = imported.friendDict
    , balanceDict = imported.balanceDict
    , transDict = imported.transDict
    , pref =
        { lang = imported.language
        , fiat = imported.fiat
        , rates = Nothing
        , locale = locale
        }
    }


friendDecoder : Decode.Decoder Friend
friendDecoder =
    succeed Friend
        |> required "id" string
        |> hardcoded Accept
        |> hardcoded Nothing


languageDecoder : Decode.Decoder Language
languageDecoder =
    string
        |> Decode.andThen
            (\str ->
                case str of
                    "Es" ->
                        succeed Es

                    "Fr" ->
                        succeed Fr

                    "En" ->
                        succeed En

                    _ ->
                        succeed Es
            )


userDecoder : Decode.Decoder ImportedUser
userDecoder =
    succeed ImportedUser
        |> required "userId" string
        |> required "name" string
        |> required "language" languageDecoder
        |> required "fiat" fiatDecoder
        |> optional "friends" (list friendDecoder) []
        |> hardcoded empty
        |> hardcoded empty
        |> hardcoded empty


decodeUser : Locale -> Decode.Value -> Maybe User
decodeUser locale json =
    case Decode.decodeValue userDecoder json of
        Ok value ->
            value |> userFromImported locale |> Just

        Err _ ->
            Nothing


decodeUsers : Locale -> Decode.Value -> List User
decodeUsers locale json =
    case Decode.decodeValue (list userDecoder) json of
        Ok value ->
            value |> List.map (userFromImported locale)

        Err _ ->
            []



-- Sort users by how much they own of your fiat


userSorter : String -> BalanceDict -> Friend -> Friend -> Order
userSorter userId balanceDict friend1 friend2 =
    let
        getValue =
            \friend ->
                Dict.get friend.id balanceDict
                    |> Maybe.map .have
                    |> Maybe.map (List.filter (\moneyPerson -> moneyPerson.fiat == userId))
                    |> Maybe.andThen List.head
                    |> Maybe.map .qty
                    |> Maybe.withDefault 0
    in
    flippedComparison (getValue friend1) (getValue friend2)


flippedComparison a b =
    case compare a b of
        LT ->
            GT

        EQ ->
            EQ

        GT ->
            LT


decodeManyTrans : Decode.Value -> TransDict
decodeManyTrans json =
    case Decode.decodeValue (list transDecoder) json of
        Ok value ->
            fromList (List.map (\b -> ( b.id, b )) value)

        Err _ ->
            empty


getFriendNameFromDict : FriendDict -> String -> String
getFriendNameFromDict dict id =
    case get id dict of
        Nothing ->
            "⋯"

        Just usr ->
            usr.name


getUserName : User -> String -> String
getUserName user id =
    if id == user.userId then
        user.name

    else
        case get id user.friendDict of
            Nothing ->
                "⋯"

            Just usr ->
                usr.name
