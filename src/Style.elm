module Style exposing (..)

import Element exposing (Attribute, Element, alignTop, centerX, el, fill, fillPortion, height, minimum, none, padding, paddingEach, paddingXY, rgb255, row, spacing, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Html.Attributes exposing (style)


edges =
    { top = 0
    , right = 0
    , bottom = 0
    , left = 0
    }



-- Pallete


cBG1 =
    colorAlmostWhite


cBG2 =
    colorWhite


cBG3 =
    colorSilver


cBG4 =
    colorAqua


cBGCard =
    colorWhite


cGrey =
    colorDarkGrey


cText1 =
    colorBlack


cText2 =
    colorLigthBlack


cText3 =
    colorWhite


cNegative =
    colorRed


cPositive =
    colorGreen


cButton =
    colorLigthGreen


cButtonBorder =
    colorButtonBorder


cAcceptButton =
    colorGreen


cRejectButton =
    colorRed


cGold =
    rgb255 254 177 63


cDarkGold =
    rgb255 242 131 23


cGoldString =
    "#feb13f"



-- End Pallete


colorWhite =
    rgb255 255 255 255


colorAlmostWhite =
    rgb255 240 240 240



--    rgb255 245 245 245


colorSilver =
    rgb255 196 203 202


colorDarkGrey =
    rgb255 80 80 80


colorLigthBlack =
    rgb255 50 50 50


colorBlack =
    rgb255 10 15 13


colorGreen =
    rgb255 29 179 122


colorRed =
    rgb255 252 64 68


colorLigthGreen =
    rgb255 20 192 154


colorButtonBorder =
    rgb255 0 165 130


colorAqua =
    rgb255 170 230 230



--


defaultFontSize =
    16


fontSizeOfDefault : Float -> Attribute msg
fontSizeOfDefault ratio =
    Font.size <| round <| (defaultFontSize * ratio)


navBarGradient : Attribute msg
navBarGradient =
    Background.gradient
        { angle = 1.5
        , steps = [ cDarkGold, cGold ]
        }


buttonBase : List (Attribute msg)
buttonBase =
    [ Font.size defaultFontSize
    , Font.color cText1
    , padding 5
    , Border.rounded 5
    , Border.solid
    ]


buttonPrimary : List (Attribute msg)
buttonPrimary =
    buttonBase
        ++ [ Background.color cButton
           , Border.color cButtonBorder
           , Font.color cText3
           ]


infoIcon : List (Attribute msg)
infoIcon =
    [ paddingXY 6 0
    , fontSizeOfDefault 0.7
    , Font.italic
    , Font.bold
    , Border.rounded 10
    , Font.color cText1
    , Background.color cGrey
    ]


buttonReject : List (Attribute msg)
buttonReject =
    buttonBase
        ++ [ Background.color cNegative
           , Border.color cButtonBorder
           ]


buttonAccept : List (Attribute msg)
buttonAccept =
    buttonBase
        ++ [ Background.color cPositive
           , Border.color cButtonBorder
           ]


disabledPrimary : List (Attribute msg)
disabledPrimary =
    buttonBase
        ++ [ Background.color cGrey
           , Border.color cGrey
           ]


buttonContact : List (Attribute msg)
buttonContact =
    buttonBase
        ++ [ Background.color cGold
           , Border.color cButtonBorder
           , Font.color cText3
           ]


buttonContactUnselected : List (Attribute msg)
buttonContactUnselected =
    buttonBase
        ++ [ Background.color colorSilver
           , Border.color colorSilver
           ]


buttonChangeCurrency : List (Attribute msg)
buttonChangeCurrency =
    buttonContact
        ++ [ fontSizeOfDefault 1.1
           ]



-- TEXT


page : List (Attribute msg)
page =
    [ width fill
    , Background.color cBG1
    , spacing 30
    ]


pageTitle : List (Attribute msg)
pageTitle =
    [ fontSizeOfDefault 1.2
    , centerX
    , Font.family
        [ Font.typeface "Open Sans"
        , Font.sansSerif
        , Font.monospace
        ]
    , Font.letterSpacing 4
    , Font.color cText1
    , Font.center
    , paddingEach { edges | top = 10 }
    ]


backgroundTitle : List (Attribute msg)
backgroundTitle =
    [ centerX
    , Font.size defaultFontSize
    , Font.bold
    , Font.color colorDarkGrey
    , paddingXY 15 10
    ]


warningText : List (Attribute msg)
warningText =
    [ Font.size defaultFontSize
    , Font.italic
    , paddingEach { edges | left = 10 }
    ]


infoText : List (Attribute msg)
infoText =
    [ Font.size defaultFontSize
    , Font.italic
    , paddingEach { edges | left = 10 }
    ]


disabledInput : List (Attribute msg)
disabledInput =
    [ Background.color cGrey
    ]



-- Page wide


pageFormat : List (Attribute msg)
pageFormat =
    [ Font.size defaultFontSize
    , width fill
    , Background.color cBG2
    , padding 15
    , spacing 15
    , centerX
    ]


welcomeScreen : List (Attribute msg)
welcomeScreen =
    [ Font.size defaultFontSize
    , width fill
    , Background.color cBG1
    , padding 20
    , spacing 30
    , centerX
    ]


content : List (Attribute msg)
content =
    [ centerX
    , width fill
    ]


marginize : Element msg -> Element msg
marginize elem =
    row
        [ width fill, paddingXY 15 0 ]
        [ el [] none
        , el content elem
        , el [] none
        ]


card : List (Attribute msg)
card =
    [ padding 10
    , centerX
    , width fill
    , Font.color cText1
    , Background.color cBGCard
    , Border.rounded 5
    , Border.shadow
        { offset = ( 1, 2 )
        , size = 1
        , blur = 4
        , color = cBG3
        }
    ]


type SendSelectionKind
    = Initial
    | Selected
    | Unselected


sendSelection : SendSelectionKind -> List (Attribute msg)
sendSelection kind =
    let
        minHeight =
            case kind of
                Initial ->
                    [ height
                        (fill
                            |> minimum 150
                        )
                    ]

                _ ->
                    []

        yPad =
            case kind of
                Initial ->
                    20

                Selected ->
                    10

                Unselected ->
                    2

        portion =
            case kind of
                Initial ->
                    5

                Selected ->
                    10

                Unselected ->
                    2

        border =
            case kind of
                Initial ->
                    5

                Selected ->
                    0

                Unselected ->
                    5
    in
    minHeight
        ++ [ Background.color cButton
           , alignTop
           , Font.center
           , Font.color cText3
           , paddingXY 10 yPad
           , Border.rounded border
           , width (fillPortion portion)
           ]



-- Fiat


fiatFormat : List (Attribute msg)
fiatFormat =
    [ Background.color cBG4
    , Font.color cText1
    , Font.regular
    , padding 2
    ]



-- HTML


htmlSelect =
    [ style "height" "calc(1em + 24px)"
    , style "background" "white"
    , style "font-size" "16px"
    , style "border-radius" "3px"
    , style "border-width" "1px"
    , style "border-color" "rgba(186,189,182,1)"
    ]
