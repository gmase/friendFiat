module Send exposing (Kind(..), Model, Msg(..), doSendMoney, filterContacts, init, modelWithoutTo, reset, update, updateWithUser, view)

import Dict exposing (get)
import Element exposing (Element, alpha, centerX, centerY, column, el, fill, fillPortion, height, html, inFront, none, padding, paragraph, row, spacing, text, width)
import Element.Background as Background
import Element.Font as Font
import Element.Input as Input
import Gold exposing (GoldAndFiat, goldFiatToFiatString, goldFiatToString, stringToGold)
import Html
import Html.Attributes exposing (id, readonly, style, value)
import I18n exposing (Texto(..))
import Money exposing (Money, MoneyPerson)
import Ports exposing (copyToClipboard, sendMoney)
import Pref exposing (getGoldPrinter)
import Qr exposing (qrCodeView)
import SendWidget
import Style
import User exposing (User)
import Utils exposing (appUrl)


type Kind
    = Select
    | Contact
    | Check


type alias JustSent =
    { qty : Int
    , concept : String
    }


type alias Model =
    { user : Maybe User
    , sendWidgetModel : SendWidget.Model
    , addFriend : Bool
    , linkText : String
    , textCopied : Bool
    , kind : Kind
    , justSent : Maybe JustSent
    , aboutToSend : Bool
    }



-- INIT


init : Model
init =
    { user = Nothing
    , sendWidgetModel = SendWidget.init
    , addFriend = True
    , linkText = ""
    , textCopied = False
    , kind = Select
    , justSent = Nothing
    , aboutToSend = False
    }


updateWithUser : Model -> User -> Model
updateWithUser model user =
    { model
        | user = Just user
        , sendWidgetModel = SendWidget.updateWithUser model.sendWidgetModel user
    }


modelWithoutTo : Model -> Model
modelWithoutTo model =
    { model | sendWidgetModel = SendWidget.clearTo model.sendWidgetModel }


reset : Model -> Model
reset model =
    { model | sendWidgetModel = SendWidget.clearQtyConcept model.sendWidgetModel }



-- UPDATE


type Msg
    = NoOp
    | GotWidgetMsg SendWidget.Msg
    | SendMoney
    | ReceivedTransferId String
    | DoCopyText
    | DeleteLink
    | ChangeKind Kind
    | AboutToSend
    | CancelSending


getFiat : Maybe User -> Int -> GoldAndFiat
getFiat user =
    user |> Maybe.map .pref |> getGoldPrinter


getConceptAndFiat : Model -> String
getConceptAndFiat model =
    (model.sendWidgetModel.qty
        |> stringToGold
        |> getFiat model.user
        |> goldFiatToFiatString
    )
        ++ " "
        ++ model.sendWidgetModel.concept


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotWidgetMsg subMsg ->
            let
                ( subModel, subCmd ) =
                    SendWidget.update subMsg model.sendWidgetModel
            in
            ( { model | sendWidgetModel = subModel }, Cmd.none )

        SendMoney ->
            let
                justSent =
                    { qty = Maybe.withDefault 0 (String.toInt model.sendWidgetModel.qty), concept = model.sendWidgetModel.concept }

                stringTo =
                    (case model.kind of
                        Check ->
                            Nothing

                        _ ->
                            model.sendWidgetModel.to
                    )
                        |> Maybe.withDefault ""
            in
            ( { model
                | justSent = Just justSent
                , sendWidgetModel = SendWidget.clearQtyConcept model.sendWidgetModel
                , aboutToSend = False
              }
            , doSendMoney
                model.user
                stringTo
                (stringToGold model.sendWidgetModel.qty)
                (getConceptAndFiat model)
                model.kind
            )

        ReceivedTransferId transferId ->
            let
                linkText =
                    case model.kind of
                        Check ->
                            transferId

                        _ ->
                            ""
            in
            ( { model | linkText = linkText }, Cmd.none )

        DoCopyText ->
            ( { model | textCopied = True }, copyToClipboard () )

        DeleteLink ->
            ( { model | linkText = "", textCopied = False }, Cmd.none )

        ChangeKind kind ->
            ( { model | kind = kind, sendWidgetModel = SendWidget.clearTo model.sendWidgetModel }, Cmd.none )

        AboutToSend ->
            ( { model | aboutToSend = True }, Cmd.none )

        CancelSending ->
            ( { model | aboutToSend = False }, Cmd.none )


doSendMoney : Maybe User -> String -> Money -> String -> Kind -> Cmd msg
doSendMoney user to qty concept kind =
    let
        kindString =
            case kind of
                Check ->
                    "check"

                _ ->
                    "target"
    in
    sendMoney
        { to = to
        , amount = getMoneyPersons user to qty
        , status = "created"
        , kind = kindString
        , addFriendSender = True
        , concept = concept
        }



-- VIEW


view : (Int -> GoldAndFiat) -> (Bool -> Float -> Float) -> (Texto -> String) -> Model -> Element Msg
view goldPrinter converter itext model =
    column Style.page
        [ el Style.pageTitle (Element.text <| itext <| EnviarDinero)
        , showBasedOnKind goldPrinter converter itext model
        ]


showBasedOnKind goldPrinter converter itext model =
    case model.kind of
        Select ->
            showSelectKind itext

        Contact ->
            column [ width fill ]
                [ showOpenedSelectKind itext model.kind
                , showSendToContact goldPrinter converter itext model
                ]

        Check ->
            column [ width fill ]
                [ showOpenedSelectKind itext model.kind
                , showSendCheck goldPrinter converter itext model
                ]


showSendToContact goldPrinter converter itext model =
    column (List.map inFront (showConfirmSending itext model.aboutToSend) ++ Style.pageFormat)
        [ Element.map GotWidgetMsg (SendWidget.printSendToContact converter itext model.sendWidgetModel)
        , showComputedOptions goldPrinter itext model
        ]


showSendCheck goldPrinter converter itext model =
    let
        name =
            case model.user of
                Nothing ->
                    ""

                Just smth ->
                    smth.name
    in
    column (List.map inFront (showConfirmSending itext model.aboutToSend) ++ Style.pageFormat)
        [ Element.map GotWidgetMsg (SendWidget.printSendCheck converter itext model.sendWidgetModel)
        , sendButton itext model
        , transactionLink goldPrinter itext name model.justSent model.linkText model.textCopied
        ]


showConfirmSending itext show =
    if not show then
        [ none ]

    else
        [ column
            [ width fill
            , height fill
            , Background.color Style.cGrey
            , alpha 0.5
            ]
            [ none ]
        , column [ spacing 20, Background.color Style.cBG1, padding 20, centerX, centerY ]
            [ paragraph [ centerX ] [ Element.text <| itext ConfirmarEnvio ]
            , row [ spacing 30 ]
                [ Input.button Style.buttonPrimary { onPress = Just SendMoney, label = Element.text <| itext Enviar }
                , Input.button Style.buttonReject { onPress = Just CancelSending, label = Element.text <| itext Cancelar }
                ]
            ]
        ]


showSelectKind itext =
    row
        [ width fill ]
        [ Input.button
            (Style.sendSelection Style.Initial)
            { onPress = Just (ChangeKind Contact)
            , label = paragraph [] [ Element.text <| itext <| AContacto ]
            }
        , Element.el [ width (fillPortion 1) ] (Element.text "")
        , Input.button
            (Style.sendSelection Style.Initial)
            { onPress = Just (ChangeKind Check)
            , label = paragraph [] [ Element.text <| itext <| Cheque ]
            }
        ]


showOpenedSelectKind itext kind =
    let
        ( contactStyle, checkStyle ) =
            case kind of
                Contact ->
                    ( Style.Selected, Style.Unselected )

                _ ->
                    ( Style.Unselected, Style.Selected )

        ( contactText, checkText ) =
            case kind of
                Contact ->
                    ( itext <| AContacto, "▽" )

                _ ->
                    ( "▽", itext <| Cheque )
    in
    row
        [ width fill ]
        [ Input.button
            (Style.sendSelection contactStyle)
            { onPress = Just (ChangeKind Contact), label = paragraph [] [ Element.text contactText ] }
        , Element.el [ width (fillPortion 1) ] (Element.text "")
        , Input.button
            (Style.sendSelection checkStyle)
            { onPress = Just (ChangeKind Check), label = paragraph [] [ Element.text checkText ] }
        ]


showComputedOptions : (Int -> GoldAndFiat) -> (Texto -> String) -> Model -> Element Msg
showComputedOptions goldPrinter itext model =
    case model.user of
        Just smth ->
            column [ width fill, spacing 10 ]
                [ el [ centerX ] (Element.text <| itext <| DetalleEnviar)
                , showDetail goldPrinter itext smth (getMoneyPersons model.user (Maybe.withDefault "" model.sendWidgetModel.to) (stringToGold model.sendWidgetModel.qty))
                , sendButton itext model
                ]

        Nothing ->
            none


allReady model =
    let
        qty =
            String.toFloat model.sendWidgetModel.qty
    in
    case qty of
        Nothing ->
            False

        Just smt ->
            smt
                > 0
                && (model.kind
                        == Check
                        || (case model.sendWidgetModel.to of
                                Nothing ->
                                    False

                                _ ->
                                    True
                           )
                   )


getMoneyPersons : Maybe User -> String -> Money -> List MoneyPerson
getMoneyPersons user to qty =
    let
        uid =
            case user of
                Nothing ->
                    "no-issuer"

                Just smth ->
                    smth.userId

        have =
            case user of
                Nothing ->
                    []

                Just smth ->
                    let
                        toFriends =
                            case get to smth.friendDict of
                                Just f ->
                                    to :: f.friends

                                Nothing ->
                                    []
                    in
                    case get uid smth.balanceDict of
                        Just balance ->
                            filterContacts balance.have to toFriends

                        Nothing ->
                            []
    in
    List.reverse <|
        getUntilAmountIsReached
            uid
            have
            []
            qty


filterContacts : List MoneyPerson -> String -> List String -> List MoneyPerson
filterContacts have to friends =
    have
        |> List.filter
            (\x -> List.member x.fiat friends)
        |> List.sortWith
            (\a b ->
                if a.fiat == to then
                    LT

                else if b.fiat == to then
                    GT

                else
                    EQ
            )



-- 1 to fiat
-- 2 friends in common fiat
-- 3 my fiat


getUntilAmountIsReached : String -> List MoneyPerson -> List MoneyPerson -> Money -> List MoneyPerson
getUntilAmountIsReached myId have moneyPerson left =
    if left == 0 then
        moneyPerson

    else
        case have of
            [] ->
                { fiat = myId, qty = left } :: moneyPerson

            x :: xs ->
                if x.qty < left then
                    getUntilAmountIsReached myId xs (x :: moneyPerson) (left - x.qty)

                else
                    { x | qty = left } :: moneyPerson


showDetail goldPrinter itext user moneyPersons =
    if List.isEmpty moneyPersons then
        none

    else
        column [ centerX ]
            (List.map
                (\m ->
                    row []
                        [ paragraph []
                            [ Element.text <| itext <| DetalleDe (goldFiatToString <| goldPrinter <| m.qty) (User.getUserName user m.fiat) ]
                        ]
                )
                moneyPersons
            )


sendButton itext model =
    if allReady model then
        Input.button (centerX :: Style.buttonPrimary) { onPress = Just AboutToSend, label = Element.text <| itext Enviar }

    else
        Input.button (centerX :: Style.disabledPrimary) { onPress = Just NoOp, label = Element.text <| itext Enviar }


transactionLink : (Int -> GoldAndFiat) -> (Texto -> String) -> String -> Maybe JustSent -> String -> Bool -> Element Msg
transactionLink goldPrinter itext from justSent linkText textCopied =
    case justSent of
        Nothing ->
            none

        Just sentData ->
            if String.isEmpty linkText then
                none

            else
                let
                    url =
                        buildUrl linkText
                in
                column [ centerX ]
                    [ paragraph []
                        [ el [ Font.bold ] (text <| itext TransferenciaRealizada)
                        , text <| itext ComparteLink
                        ]
                    , qrCodeView url |> html |> el [ centerX ]
                    , row [ centerX, spacing 10 ]
                        [ phantomText <| itext (TeHaEnviado from (goldFiatToString (goldPrinter sentData.qty)) sentData.concept url)
                        , column []
                            [ Input.button Style.buttonPrimary { onPress = Just DoCopyText, label = Element.text <| itext Copiar }
                            , showDeleteLastTransferButton itext textCopied
                            ]
                        ]
                    ]


buildUrl : String -> String
buildUrl linkText =
    appUrl ++ "collect?t=" ++ linkText


phantomText : String -> Element Msg
phantomText textToCopy =
    html
        (Html.textarea
            [ id "copy", value textToCopy, readonly True, style "outline" "none", style "resize" "none", style "background-color" "grey", style "min-width" "200px" ]
            [ Html.text textToCopy ]
        )


showDeleteLastTransferButton : (Texto -> String) -> Bool -> Element Msg
showDeleteLastTransferButton itext show =
    if show then
        Input.button Style.buttonPrimary { onPress = Just DeleteLink, label = Element.text <| itext <| BorrarMensaje }

    else
        none
