module Friends exposing (Model, Msg(..), init, update, view)

import Dict exposing (empty, get)
import Element exposing (Element, alignRight, centerX, column, el, fill, fillPortion, html, none, paddingXY, paragraph, row, spacing, spacingXY, width)
import Element.Font as Font
import Element.Input as Input
import Gold exposing (GoldAndFiat, goldFiatToTitle)
import Html
import Html.Attributes exposing (id, readonly, style, value)
import I18n exposing (Texto(..))
import Ports exposing (copyToClipboard, sendInvite)
import Qr exposing (qrCodeView)
import Style
import User exposing (Friend, FriendDict, User)
import Utils exposing (appUrl)



-- MODEL


type alias Model =
    { friends : List Friend
    , friendDict : FriendDict
    , user : Maybe User
    , inviteLink : Maybe String
    , copied : Bool
    }



-- INIT


init : List Friend -> Model
init friends =
    { friends = friends
    , friendDict = empty
    , user = Nothing
    , inviteLink = Nothing
    , copied = False
    }


type Msg
    = GenerateInvite
    | DoCopyText
    | ReceivedInviteId String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GenerateInvite ->
            ( { model | copied = False }, doInvite )

        DoCopyText ->
            ( { model | copied = True }, copyToClipboard () )

        ReceivedInviteId inviteId ->
            ( { model | inviteLink = Just inviteId }, Cmd.none )


doInvite : Cmd msg
doInvite =
    sendInvite
        { to = ""
        , amount = []
        , status = "created"
        , kind = "invite"
        , addFriendSender = True
        , concept = ""
        }


view : (Int -> GoldAndFiat) -> (Texto -> String) -> Model -> Element Msg
view goldPrinter itext model =
    let
        balanceDict =
            case model.user of
                Nothing ->
                    empty

                Just smth ->
                    smth.balanceDict

        ( userId, userName ) =
            case model.user of
                Nothing ->
                    ( "", "" )

                Just smth ->
                    ( smth.userId, smth.name )
    in
    column (paddingXY 0 15 :: Style.page)
        [ model.friends
            |> List.map (paintFriend goldPrinter itext model.friendDict balanceDict userId)
            |> column [ width fill, spacingXY 0 10 ]
            |> Style.marginize
        , paintInvitePanel itext model userName
        ]


paintInvitePanel itext model userName =
    column [ width fill, paddingXY 0 20 ]
        [ Input.button (centerX :: Style.buttonPrimary)
            { onPress = Just GenerateInvite, label = Element.text <| itext <| NuevaInvitacion }
        , case model.inviteLink of
            Nothing ->
                none

            Just smth ->
                paintInviteLinkPlusCopyButton itext userName smth model.copied
        ]


buildUrl : String -> String
buildUrl link =
    appUrl ++ "invite?t=" ++ link


paintInviteLinkPlusCopyButton itext userName link copied =
    column [ centerX ]
        [ link |> buildUrl |> qrCodeView |> html |> el [ centerX ]
        , link |> buildUrl |> QuiereSerTuAmigo userName |> itext |> paintInviteLink
        , printCopyButton itext copied
        ]


printCopyButton itext copied =
    let
        buttonStyle =
            if copied then
                Style.disabledPrimary

            else
                Style.buttonPrimary

        texto =
            if copied then
                Copiado

            else
                Copiar
    in
    Input.button buttonStyle
        { onPress = Just DoCopyText, label = Element.text <| itext texto }


paintInviteLink : String -> Element Msg
paintInviteLink textToCopy =
    html
        (Html.textarea
            [ id "copy"
            , value textToCopy
            , readonly True
            , style "overflow" "hidden"
            , style "font-size" "0.6rem"
            , style "outline" "none"
            , style "resize" "none"
            , style "background-color" "lightgray"
            , style "min-width" "20rem"
            , style "min-height" "3rem"
            , style "margin-top" "1rem"
            , style "margin-bottom" "1rem"
            ]
            [ Html.text textToCopy ]
        )


paintFriend goldPrinter itext friendDict balanceDict userId friend =
    let
        tempName =
            get friend.id friendDict

        friendName =
            case tempName of
                Nothing ->
                    "⋯"

                Just smth ->
                    smth.name

        tempBalance =
            get friend.id balanceDict

        balance =
            case tempBalance of
                Nothing ->
                    none

                Just smth ->
                    paintBalance goldPrinter itext smth userId
    in
    row Style.card
        [ paragraph ((width <| fillPortion <| 2) :: Style.warningText) [ Element.text friendName ]
        , balance
        ]



-- paintBalance : (Int -> GoldAndFiat) -> (Texto -> String) -> Ledger -> String -> Element Msg


paintBalance goldPrinter itext balance userId =
    let
        yourMoney =
            case List.filter (\x -> x.fiat == userId) balance.have of
                x :: xs ->
                    if x.qty == 0 then
                        el [] (Element.text "-")

                    else
                        goldFiatToTitle <| goldPrinter <| x.qty

                [] ->
                    el [] (Element.text "-")
    in
    row [ alignRight, Style.fontSizeOfDefault 0.7, spacing 8, width <| fillPortion <| 2 ]
        [ column [ spacing 5, width <| fillPortion <| 1 ]
            [ el [ centerX, Font.italic ] (Element.text <| itext <| DeTuMoneda)
            , el [ centerX, Font.bold ] yourMoney
            ]
        ]
