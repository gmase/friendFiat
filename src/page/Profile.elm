module Profile exposing (Model, Msg(..), initWithUser, update, view)

import Element exposing (Element, alignLeft, centerX, column, el, fill, html, none, paddingXY, paragraph, spacing, text, width)
import Element.Font as Font
import Element.Input as Input
import Gold exposing (Fiat, availableFiats, defaultFiat, fiatToText, getFiatFromCode)
import Html exposing (select)
import Html.Attributes as Attr exposing (value)
import Html.Events exposing (onInput)
import I18n exposing (Language(..), Texto(..), showLanguageIcon)
import Ports exposing (ProfileToUpdate, sendNewProfile)
import Style
import User exposing (User, getInitUser, updateUserWithPrefs)


maxUserNameLegth =
    35


cutName =
    String.left maxUserNameLegth



-- MODEL


type alias NewUserProfile =
    { name : String
    , lang : Language
    , fiat : Fiat
    }


type alias Model =
    { user : User
    , newUserProfile : NewUserProfile
    }



-- INIT


initWithUser : Maybe User -> Model
initWithUser user =
    case user of
        Nothing ->
            { user = getInitUser
            , newUserProfile = { name = "", lang = Es, fiat = defaultFiat }
            }

        Just smt ->
            { user = smt
            , newUserProfile = { name = smt.name, lang = smt.pref.lang, fiat = smt.pref.fiat }
            }



-- UPDATE


type Msg
    = ChangeNewUserProfile NewUserProfile
    | UpdateProfile User NewUserProfile


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeNewUserProfile newUser ->
            ( { model | newUserProfile = newUser }, Cmd.none )

        UpdateProfile user newUserProfile ->
            ( { model | user = updateUserFromNewUser newUserProfile user }
            , newUserProfile |> buildProfileToUpdate user |> sendNewProfile
            )


updateUserFromNewUser : NewUserProfile -> User -> User
updateUserFromNewUser newUser user =
    let
        tempUser =
            { user | name = newUser.name }
    in
    updateUserWithPrefs tempUser newUser.lang newUser.fiat


buildProfileToUpdate : User -> NewUserProfile -> ProfileToUpdate
buildProfileToUpdate user newUser =
    let
        name =
            if String.isEmpty newUser.name then
                user.name

            else
                newUser.name

        language =
            case newUser.lang of
                Es ->
                    "Es"

                Fr ->
                    "Fr"

                En ->
                    "En"
    in
    { name = name
    , language = language
    , fiat = newUser.fiat.code
    }


doChangeNewUserProfile : NewUserProfile -> String -> Msg
doChangeNewUserProfile profile newName =
    ChangeNewUserProfile { profile | name = cutName newName }


doChangeLanguage : NewUserProfile -> Language -> Bool -> Msg
doChangeLanguage profile lang _ =
    ChangeNewUserProfile { profile | lang = lang }


doChangeFiat : NewUserProfile -> String -> Msg
doChangeFiat profile fiatCode =
    ChangeNewUserProfile { profile | fiat = getFiatFromCode fiatCode }



-- VIEW


view : (Texto -> String) -> Model -> Element Msg
view itext model =
    column Style.page
        [ --el Style.pageTitle (Element.text <| itext Perfil)
          column
            Style.pageFormat
            [ showUserData itext model
            , Input.button (centerX :: Style.buttonPrimary)
                { onPress = Just (UpdateProfile model.user model.newUserProfile)
                , label = Element.text <| itext ActualizarPerfil
                }
            ]
        ]


showUserData : (Texto -> String) -> Model -> Element Msg
showUserData itext model =
    column []
        [ showNameInput itext model
        , showLanguageSelector itext model
        , showFiatSelector itext model
        ]


showNameInput itext model =
    column [ width fill ]
        [ Input.text []
            { label = itext Nombre |> Element.text |> Input.labelAbove []
            , placeholder = Nothing
            , text = cutName model.newUserProfile.name
            , onChange = doChangeNewUserProfile model.newUserProfile
            }
        , if model.user.name /= "new user" then
            none

          else
            [ itext IntruduceTuNombre |> Element.text ] |> paragraph Style.warningText
        ]


showLanguageSelector itext model =
    column [ paddingXY 5 25, spacing 20 ]
        [ showLanguageCheckbox itext model.newUserProfile Es
        , showLanguageCheckbox itext model.newUserProfile Fr
        , showLanguageCheckbox itext model.newUserProfile En
        ]


showLanguageCheckbox itext newUserProfile lang =
    let
        checked =
            lang == newUserProfile.lang

        style =
            if checked then
                [ Font.bold, Style.fontSizeOfDefault 1.1 ]

            else
                []
    in
    Input.checkbox style
        { onChange = doChangeLanguage newUserProfile lang
        , icon = showLanguageIcon lang
        , checked = checked
        , label =
            Input.labelRight []
                (text <| languageName itext lang)
        }


languageName itext lang =
    case lang of
        Es ->
            itext Espanol

        Fr ->
            itext Frances

        En ->
            itext Ingles


showFiatSelector itext model =
    column [ width fill, spacing 10 ]
        [ el [ alignLeft ] (Element.text <| itext MonedaReferencia)
        , html
            (select
                (onInput (doChangeFiat model.newUserProfile) :: Style.htmlSelect)
                (List.map
                    (showFiat
                        model.newUserProfile.fiat
                    )
                    availableFiats
                )
            )
            |> el [ alignLeft, width fill ]
        ]


showFiat : Fiat -> Fiat -> Html.Html msg
showFiat selectedOption fiat =
    let
        isSelected =
            selectedOption.code == fiat.code
    in
    Html.option [ Attr.selected isSelected, value fiat.code ]
        [ Html.text <| fiatToText <| fiat ]
