module SignOn exposing (Model, Msg(..), init, update, view)

import Asset exposing (logo, src)
import Element exposing (Element, alignLeft, alignRight, alignTop, centerX, centerY, column, el, fill, height, html, image, padding, paddingXY, paragraph, px, row, spacing, text, width)
import Element.Events exposing (onClick)
import Element.Font as Font
import Element.Input as Input
import I18n exposing (Language(..), Texto(..), itext, showLanguageIcon)
import Loading exposing (LoaderType(..), defaultConfig)
import Ports exposing (EmailAndLink, sendEmail)
import Style



-- MODEL


type InfoScreen
    = SignIn
    | Welcome
    | How
    | What


type Status
    = NoUser InfoScreen
    | EmailSent
    | Waiting


type alias EmailAddress =
    String


type alias Model =
    { email : Maybe EmailAddress
    , status : Status
    , collectUrl : String
    , lang : Language
    }



-- INIT


init : Language -> Maybe String -> Model
init language collectUrl =
    { email = Nothing
    , status = Waiting
    , collectUrl = Maybe.withDefault "/profile" collectUrl
    , lang = language
    }


type Msg
    = SendEmail (Maybe EmailAddress)
    | ChangeEmail (Maybe String)
    | GotNoUser
    | GoTo InfoScreen
    | ChangeLanguage Language Bool


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SendEmail email ->
            case email of
                Nothing ->
                    ( model, Cmd.none )

                Just smth ->
                    ( { model | status = EmailSent }, sendEmail (EmailAndLink smth model.collectUrl) )

        ChangeEmail newEmail ->
            ( { model | email = newEmail }, Cmd.none )

        GotNoUser ->
            ( { model | status = NoUser Welcome }, Cmd.none )

        GoTo infoScreen ->
            ( { model | status = NoUser infoScreen }, Cmd.none )

        ChangeLanguage lang _ ->
            ( { model | lang = lang }, Cmd.none )



-- VIEW


doChangeEmail : String -> Msg
doChangeEmail newEmail =
    ChangeEmail (Just newEmail)


getItext : Model -> (Texto -> String)
getItext model =
    itext model.lang "$"


view : Model -> Element Msg
view model =
    let
        itext =
            getItext model
    in
    case model.status of
        NoUser infoScreen ->
            showNoUser itext infoScreen model.email model.lang

        Waiting ->
            column [ centerX, centerY, padding 100 ]
                [ el [] <|
                    html <|
                        Loading.render
                            Bars
                            { defaultConfig
                                | size = 50
                                , color = Style.cGoldString
                            }
                            Loading.On
                ]

        EmailSent ->
            column
                Style.pageFormat
                [ paragraph Style.infoText
                    [ Element.text <| itext EmailEnviado ]
                ]


showNoUser : (Texto -> String) -> InfoScreen -> Maybe EmailAddress -> Language -> Element Msg
showNoUser itext infoScreen email lang =
    case infoScreen of
        SignIn ->
            showSignIn itext email

        Welcome ->
            showWelcome itext lang

        How ->
            showHow itext lang

        What ->
            showWhat itext lang


showWelcome itext lang =
    column
        Style.welcomeScreen
        [ showHeader itext lang
        , paragraph [ Style.fontSizeOfDefault 2, Font.bold, Font.color Style.cText2 ]
            [ BienvenidaTitulo1 |> itext |> Element.text |> el []
            , BienvenidaTitulo2 |> itext |> Element.text |> el [ Font.color Style.cDarkGold ]
            , BienvenidaTitulo3 |> itext |> Element.text |> el []
            ]
        , paragraph [ Style.fontSizeOfDefault 1.2 ]
            [ BienvenidaSubtitulo |> itext |> Element.text |> el []
            ]
        , Input.button (centerX :: Style.buttonPrimary)
            { onPress = Just (GoTo How)
            , label =
                [ ComoFunciona |> itext |> Element.text, "➥" |> Element.text |> el [ centerX ] ]
                    |> column [ Font.bold, Style.fontSizeOfDefault 1.2, padding 5, spacing 5 ]
            }
        ]


showHow itext lang =
    column
        Style.welcomeScreen
        [ showHeader itext lang
        , paragraph [ Style.fontSizeOfDefault 1.2 ]
            [ ComoFuncionaSubtitulo |> itext |> Element.text |> el []
            ]
        , Input.button (centerX :: Style.buttonPrimary)
            { onPress = Just (GoTo What)
            , label =
                [ ParaQue |> itext |> Element.text, "➥" |> Element.text |> el [ centerX ] ]
                    |> column [ Font.bold, Style.fontSizeOfDefault 1.2, padding 5, spacing 5 ]
            }
        ]


showWhat itext lang =
    column
        Style.welcomeScreen
        [ showHeader itext lang
        , paragraph [ Style.fontSizeOfDefault 1.2 ]
            [ ParaQueSubtitulo |> itext |> Element.text |> el []
            ]
        , Input.button (centerX :: Style.buttonPrimary)
            { onPress = Just (GoTo SignIn)
            , label =
                [ Logeate |> itext |> Element.text |> el [ centerX ] ]
                    |> column [ Font.bold, Style.fontSizeOfDefault 1.2, padding 5, spacing 5 ]
            }
        ]


showHeader itext lang =
    row [ width fill, padding 0 ]
        [ showLogo
        , showLanguageSelector itext lang
        , Input.button (padding 0 :: alignTop :: alignRight :: Style.buttonPrimary)
            { onPress = Just (GoTo SignIn)
            , label = Logeate |> itext |> Element.text
            }
        ]


showLogo =
    column [ onClick (GoTo Welcome), alignLeft, centerY, padding 0 ]
        [ image [ height (px 80) ] { src = src logo, description = "logo" }
        ]


showSignIn itext email =
    column
        Style.welcomeScreen
        [ showLogo
        , Input.email []
            { label = Input.labelAbove [] (Element.text <| itext IntroduceEmail)
            , placeholder = Nothing
            , text = showEmailOrPlaceHolder email
            , onChange = doChangeEmail
            }
        , Input.button Style.buttonPrimary
            { onPress = Just (SendEmail email)
            , label = Element.text <| itext EnviarEmail
            }
        , paragraph Style.infoText [ Element.text <| itext RecibirasEmail ]
        ]


showEmailOrPlaceHolder : Maybe String -> String
showEmailOrPlaceHolder maybeEmail =
    case maybeEmail of
        Nothing ->
            ""

        Just smth ->
            smth


showLanguageSelector itext lang =
    row [ alignTop, alignRight, paddingXY 15 0, spacing 5 ]
        [ showLanguageCheckbox itext lang Es
        , showLanguageCheckbox itext lang Fr
        , showLanguageCheckbox itext lang En
        ]


showLanguageCheckbox itext modelLang lang =
    let
        checked =
            lang == modelLang

        style =
            if checked then
                [ Font.bold, Style.fontSizeOfDefault 1.1 ]

            else
                []
    in
    Input.checkbox style
        { onChange = ChangeLanguage lang
        , icon = showLanguageIcon lang
        , checked = checked
        , label =
            "" |> text |> Input.labelRight []
        }
