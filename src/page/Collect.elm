module Collect exposing (Kind(..), Model, Msg(..), doClaim, init, update, view)

import Element exposing (Element, column, paragraph)
import Element.Input as Input
import I18n exposing (Texto(..))
import Ports exposing (claim)
import Style



-- MODEL


type Status
    = New
    | Claimed


type Kind
    = Payment
    | Invite


type alias Model =
    { transId : String
    , status : Status
    , addFriend : Bool
    , kind : Kind
    }



-- INIT


init : Kind -> Maybe String -> Model
init kind trans =
    { transId = Maybe.withDefault "" trans
    , status = New
    , addFriend = True
    , kind = kind
    }


type Msg
    = Claim
    | ChangeAddFriend Bool


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Claim ->
            doClaim model

        ChangeAddFriend add ->
            ( { model | addFriend = add }, Cmd.none )


doClaim model =
    ( { model | status = Claimed }, claim (Ports.Claim model.transId model.addFriend) )



-- VIEW


view : (Texto -> String) -> Model -> Element Msg
view itext model =
    case model.status of
        New ->
            viewUnClaimed itext model.kind

        Claimed ->
            viewClaimed itext model.kind


viewUnClaimed itext kind =
    case kind of
        Payment ->
            column
                Style.pageFormat
                [ Input.button Style.buttonPrimary { onPress = Just Claim, label = Element.text <| itext Reclamar }
                , paragraph Style.warningText [ Element.text <| itext ImporteSeAnadira ]
                ]

        Invite ->
            column
                Style.pageFormat
                [ Input.button Style.buttonPrimary { onPress = Just Claim, label = Element.text <| itext AceptarEnvio }
                , paragraph Style.warningText [ Element.text <| itext ContactoSeAnadira ]
                ]


viewClaimed itext kind =
    case kind of
        Payment ->
            column
                Style.pageFormat
                [ paragraph Style.warningText [ Element.text <| itext ChequeReclamado ]
                ]

        Invite ->
            column
                Style.pageFormat
                [ paragraph Style.warningText [ Element.text <| itext InvitacionAceptada ]
                ]
