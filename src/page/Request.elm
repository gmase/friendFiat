module Request exposing (Model, Msg(..), Request, RequestListKind(..), checkPendingRequests, decodeManyRequests, init, modelWithoutTo, reset, update, updateWithRequests, updateWithUser, view)

import Element exposing (Element, alignRight, centerX, column, el, fill, fillPortion, none, paddingXY, paragraph, row, spacing, width)
import Element.Font as Font
import Element.Input as Input
import Gold exposing (GoldAndFiat, goldFiatToString, stringToGold)
import Html.Attributes exposing (value)
import I18n exposing (Texto(..))
import Json.Decode as Decode exposing (list, string, succeed, value)
import Json.Decode.Pipeline exposing (required)
import Ledger exposing (MoneyPersonsWithTotal, emptyMoneyPersonWithTotal, moneyPersonsWithTotalDecoder)
import Ports exposing (respondToRequest, sendRequest)
import Send
import SendWidget
import Style
import User exposing (User)


type alias Request =
    { id : String
    , from : String
    , to : String
    , concept : String
    , amountFrom : MoneyPersonsWithTotal
    , amountTo : MoneyPersonsWithTotal
    , status : Status
    }


statusDecoder : Decode.Decoder Status
statusDecoder =
    string
        |> Decode.andThen
            (\str ->
                case str of
                    "pending" ->
                        Decode.succeed Pending

                    "accepted" ->
                        Decode.succeed Accepted

                    somethingElse ->
                        Decode.succeed Rejected
            )


requestDecoder : Decode.Decoder Request
requestDecoder =
    succeed Request
        |> required "id" string
        |> required "from" string
        |> required "to" string
        |> required "concept" string
        |> required "amountFrom" moneyPersonsWithTotalDecoder
        |> required "amountTo" moneyPersonsWithTotalDecoder
        |> required "status" statusDecoder


decodeManyRequests : Decode.Value -> List Request
decodeManyRequests json =
    case Decode.decodeValue (list requestDecoder) json of
        Ok value ->
            value

        Err msg ->
            []


checkPendingRequests : List Request -> Bool
checkPendingRequests =
    List.any
        (\req ->
            case req.status of
                Pending ->
                    True

                _ ->
                    False
        )


type Status
    = Pending
    | Accepted
    | Rejected


type alias Model =
    { pendingRequests : List Request
    , myRequests : List Request
    , user : Maybe User
    , sendWidgetModel : SendWidget.Model
    }



-- INIT


init : Model
init =
    { pendingRequests = []
    , myRequests = []
    , user = Nothing
    , sendWidgetModel = SendWidget.init
    }


modelWithoutTo : Model -> Model
modelWithoutTo model =
    { model
        | sendWidgetModel = SendWidget.clearTo model.sendWidgetModel
    }


reset : Model -> Model
reset model =
    { model | sendWidgetModel = SendWidget.clearQtyConcept model.sendWidgetModel }


updateWithUser : Model -> User -> Model
updateWithUser model user =
    { model
        | user = Just user
        , sendWidgetModel = SendWidget.updateWithUser model.sendWidgetModel user
    }


updateWithRequests : RequestListKind -> Model -> List Request -> Model
updateWithRequests kind model requests =
    case kind of
        PendingRequests ->
            { model | pendingRequests = requests }

        MyRequests ->
            { model | myRequests = requests }


type RequestListKind
    = PendingRequests
    | MyRequests



-- UPDATE


type Msg
    = NoOp
    | GotWidgetMsg SendWidget.Msg
    | SendRequest
    | AcceptRequest Request
    | RejectRequest String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotWidgetMsg subMsg ->
            let
                ( subModel, subCmd ) =
                    SendWidget.update subMsg model.sendWidgetModel
            in
            ( { model | sendWidgetModel = subModel }, Cmd.none )

        SendRequest ->
            ( { model | sendWidgetModel = SendWidget.clearQtyConcept model.sendWidgetModel }, doSendRequest model.sendWidgetModel )

        AcceptRequest request ->
            ( model, doAcceptRequest model request )

        RejectRequest requestId ->
            ( model, respondToRequest { id = requestId, status = "rejected" } )


doSendRequest : SendWidget.Model -> Cmd msg
doSendRequest widgetModel =
    case widgetModel.to of
        Nothing ->
            Cmd.none

        Just smth ->
            sendRequest
                { to = smth
                , concept = widgetModel.concept
                , amountFrom = emptyMoneyPersonWithTotal
                , amountTo = { emptyMoneyPersonWithTotal | total = stringToGold widgetModel.qty }
                , status = "pending"
                }


doAcceptRequest : Model -> Request -> Cmd msg
doAcceptRequest model request =
    case model.user of
        Nothing ->
            Cmd.none

        Just usr ->
            Cmd.batch
                [ respondToRequest { id = request.id, status = "accepted" }
                , Send.doSendMoney model.user request.from request.amountTo.total request.concept Send.Contact
                ]



-- VIEW


view : (Int -> GoldAndFiat) -> (Bool -> Float -> Float) -> (Texto -> String) -> Model -> Element Msg
view goldPrinter converter itext model =
    case model.user of
        Nothing ->
            none

        Just usr ->
            column (paddingXY 0 5 :: Style.page)
                [ printRequestList goldPrinter itext PendingRequests model.pendingRequests usr
                , showSendToContact converter itext model
                , printRequestList goldPrinter itext MyRequests model.myRequests usr
                ]



-- PRINT REQUESTS


printRequestList : (Int -> GoldAndFiat) -> (Texto -> String) -> RequestListKind -> List Request -> User -> Element Msg
printRequestList goldPrinter itext requestKind requestList usr =
    let
        pendingRequests =
            List.filter (\req -> req.status == Pending) requestList

        requestPrinter =
            case requestKind of
                PendingRequests ->
                    printAcceptableRequest

                MyRequests ->
                    printMyRequest

        content =
            case pendingRequests of
                [] ->
                    paragraph Style.infoText <|
                        case requestKind of
                            PendingRequests ->
                                [ Element.text <| itext <| SinReclamacionesDeOtros ]

                            MyRequests ->
                                [ Element.text <| itext <| SinReclamacionesTuyas ]

                _ ->
                    Style.marginize <|
                        column [ spacing 15, width fill ]
                            (List.map (requestPrinter goldPrinter itext usr.friendDict) pendingRequests)
    in
    column
        [ width fill ]
        [ content
        ]


centeredLabel : String -> Element Msg
centeredLabel label =
    label |> Element.text |> el [ centerX ]


printAcceptableRequest : (Int -> GoldAndFiat) -> (Texto -> String) -> User.FriendDict -> Request -> Element Msg
printAcceptableRequest goldPrinter itext dict req =
    row
        (spacing 5 :: (Style.fontSizeOfDefault 0.8 :: Style.card))
        [ column [ 4 |> fillPortion |> width ]
            [ req.amountTo.total
                |> goldPrinter
                |> goldFiatToString
                |> TePide (User.getFriendNameFromDict dict req.from)
                |> itext
                |> Element.text
                |> List.singleton
                |> paragraph []
            , el [ Font.italic ] (Element.text req.concept)
            ]
        , Input.button ((1 |> fillPortion |> width) :: alignRight :: Style.buttonAccept)
            { onPress = Just (AcceptRequest req), label = centeredLabel <| itext AceptarRequest }
        , Input.button ((1 |> fillPortion |> width) :: alignRight :: Style.buttonReject)
            { onPress = Just (RejectRequest req.id), label = centeredLabel <| itext RechazarRequest }
        ]


printMyRequest : (Int -> GoldAndFiat) -> (Texto -> String) -> User.FriendDict -> Request -> Element Msg
printMyRequest goldPrinter itext dict req =
    row
        (spacing 5 :: (Style.fontSizeOfDefault 0.8 :: Style.card))
        [ column [ 4 |> fillPortion |> width ]
            [ req.amountTo.total
                |> goldPrinter
                |> goldFiatToString
                |> Pediste (User.getFriendNameFromDict dict req.to)
                |> itext
                |> Element.text
                |> List.singleton
                |> paragraph []
            , el [ Font.italic ] (Element.text req.concept)
            ]
        , Input.button ((1 |> fillPortion |> width) :: alignRight :: Style.buttonReject)
            { onPress = Just (RejectRequest req.id), label = centeredLabel <| itext Cancelar }
        ]



-- END PRINT REQUESTS


showSendToContact : (Bool -> Float -> Float) -> (Texto -> String) -> Model -> Element Msg
showSendToContact converter itext model =
    column Style.pageFormat
        [ itext PedirAContacto
            |> Element.text
            |> el [ Font.bold, centerX ]
        , SendWidget.printSendToContact converter itext model.sendWidgetModel |> Element.map GotWidgetMsg
        , requestButton itext model
        ]


requestButton itext model =
    if allReady model then
        Input.button (centerX :: Style.buttonPrimary) { onPress = Just SendRequest, label = Element.text <| itext Pedir }

    else
        Input.button (centerX :: Style.disabledPrimary) { onPress = Just NoOp, label = Element.text <| itext Pedir }


allReady model =
    let
        qty =
            String.toFloat model.sendWidgetModel.qty
    in
    case model.sendWidgetModel.to of
        Nothing ->
            False

        _ ->
            case qty of
                Nothing ->
                    False

                Just smt ->
                    smt > 0
