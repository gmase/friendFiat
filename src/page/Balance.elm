module Balance exposing (Model, Msg(..), init, initCmd, update, updateModelWithLedgerAndUser, updateModelWithTransDict, view)

import Asset
import Date exposing (timestampToDate)
import Dict exposing (Dict, empty, get)
import Element exposing (Attribute, Element, alignRight, alignTop, centerX, column, el, fill, fillPortion, height, image, padding, paddingXY, paragraph, px, row, spacingXY, width)
import Element.Font as Font
import Gold exposing (GoldAndFiat, goldFiatToFiatElement, goldFiatToTitle)
import I18n exposing (Texto(..))
import Ledger exposing (Ledger)
import Maybe
import Money exposing (MoneyPerson, moneyPersonToRow, sumOfMoneyPersons)
import Style
import Task
import Time
import User exposing (TransDict, User)



-- MODEL


type Status
    = MissBalance
    | MissHas
    | Loaded



-- FIXME: Duplicated data in transDict and transMoneyPersonDict


type alias Model =
    { status : Status
    , ledger : Ledger
    , user : Maybe User
    , zone : Time.Zone
    , haveDict : Dict String MoneyPerson
    , transDict : TransDict
    , transMoneyPersonDict : Dict String (Dict String MoneyPerson)
    }



-- INIT


init : Maybe String -> Model
init id =
    { status = MissBalance
    , user = Nothing
    , ledger =
        { id = Maybe.withDefault "" id
        , owe = 0
        , haveTotal = 0
        , have = []
        , lastTrans = []
        }
    , zone = Time.utc
    , haveDict = empty
    , transDict = empty
    , transMoneyPersonDict = empty
    }


updateModelWithLedgerAndUser : Model -> Maybe Ledger -> User -> Model
updateModelWithLedgerAndUser oldModel ledger newUser =
    let
        newLedger =
            Maybe.withDefault Ledger.emptyLedger ledger

        haveDict =
            Dict.fromList (List.map (\a -> ( a.fiat, a )) newLedger.have)
    in
    { oldModel
        | ledger = newLedger
        , user = Just newUser
        , haveDict = haveDict
    }


updateModelWithTransDict : Model -> TransDict -> Model
updateModelWithTransDict oldModel transDict =
    let
        transMoneyPersonDict =
            Dict.map
                (\k v ->
                    v.amount
                        |> List.map (\a -> ( a.fiat, a ))
                        |> Dict.fromList
                )
                transDict
    in
    { oldModel
        | transDict = transDict
        , transMoneyPersonDict = transMoneyPersonDict
    }


initCmd : Cmd Msg
initCmd =
    Task.perform AdjustTimeZone Time.here


type Msg
    = LoadBalance String
    | ReceivedLedger (Maybe Ledger)
    | AdjustTimeZone Time.Zone


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LoadBalance uid ->
            ( model, Cmd.none )

        ReceivedLedger ledger ->
            case ledger of
                Nothing ->
                    ( model, Cmd.none )

                Just smt ->
                    ( { model | ledger = smt }, Cmd.none )

        AdjustTimeZone newZone ->
            ( { model | zone = newZone }
            , Cmd.none
            )


getUserNameSearcher : Model -> String -> Maybe String
getUserNameSearcher model id =
    case model.user of
        Nothing ->
            Nothing

        Just usr ->
            Just (User.getUserName usr id)



-- VIEW


titleFormat : List (Attribute msg)
titleFormat =
    [ Font.bold, Font.color <| Style.cText2, centerX ]


view : (Int -> GoldAndFiat) -> (Texto -> String) -> Model -> Element Msg
view goldPrinter itext model =
    column Style.page
        [ row (spacingXY 10 0 :: (width fill :: Style.pageFormat))
            [ column [ width << fillPortion <| 1, alignTop, spacingXY 0 10 ]
                [ paragraph
                    titleFormat
                    [ Element.text <| itext MonedasDeOtros ]
                , paragraph
                    [ Font.bold, Font.color <| Style.cPositive, centerX ]
                    [ goldFiatToFiatElement [] <| goldPrinter <| model.ledger.haveTotal ]
                , showHaveDictDetail goldPrinter itext (getUserNameSearcher model) model.haveDict
                ]
            , column [ width << fillPortion <| 1, alignTop, spacingXY 0 10 ]
                [ el
                    titleFormat
                    (Element.text <| itext MonedaEmitida)
                , el [ Font.color <| Style.cNegative, centerX ] (goldFiatToTitle <| goldPrinter <| negate <| model.ledger.owe)
                ]
            ]
        , printDetailSection goldPrinter itext model
        ]


printDetailSection printGold itext model =
    column
        [ width fill, spacingXY 0 10 ]
        [ printDetail printGold itext model
        ]


printDetail printGold itext model =
    column [ spacingXY 0 15, width fill, Style.fontSizeOfDefault 1 ]
        [ printLastTransfers printGold itext model
        ]


showHaveDictDetail printGold itext userNameSearcher haveDict =
    el [ Style.fontSizeOfDefault 0.7, Font.letterSpacing 0 ]
        (showFiats [ Font.color <| Style.cPositive ] printGold userNameSearcher haveDict)


showFiats : List (Element.Attr decorative msg) -> (Int -> GoldAndFiat) -> (String -> Maybe String) -> Dict String MoneyPerson -> Element msg
showFiats amountFormat printGold userNameSearcher haveDict =
    haveDict
        |> Dict.values
        |> List.map (showDetailLine printGold userNameSearcher)
        |> column [ paddingXY 5 5, spacingXY 0 5, width fill ]


showDetailLine : (Int -> GoldAndFiat) -> (String -> Maybe String) -> MoneyPerson -> Element msg
showDetailLine printGold userNameSearcher currency =
    moneyPersonToRow printGold userNameSearcher currency


flippedComparison a b =
    case compare a.time b.time of
        LT ->
            GT

        EQ ->
            EQ

        GT ->
            LT


printLastTransfers printGold itext model =
    List.sortWith flippedComparison model.ledger.lastTrans
        |> List.map
            (printTransfer printGold itext model.zone (getUserNameSearcher model) model.transDict model.transMoneyPersonDict)
        |> column
            [ width fill, spacingXY 0 15, Style.fontSizeOfDefault 0.8 ]


type TransferKind
    = Incoming
    | Outcoming


printTransfer printGold itext zone userNameSearcher transDict transMoneyPersonDict recentTran =
    let
        transferKind =
            if recentTran.kind == "incoming" then
                Incoming

            else
                Outcoming

        transData =
            get recentTran.transId transDict

        transMoneyPerson =
            Maybe.withDefault empty (get recentTran.transId transMoneyPersonDict)

        userId =
            case transData of
                Nothing ->
                    ""

                Just data ->
                    case transferKind of
                        Incoming ->
                            data.from

                        Outcoming ->
                            data.to

        userName =
            if userId == "" then
                "⋯"

            else
                Maybe.withDefault "" (userNameSearcher userId)

        userText =
            case transferKind of
                Incoming ->
                    itext <| DeEnvio <| userName

                Outcoming ->
                    itext <| AEnvio <| userName

        concept =
            case transData of
                Nothing ->
                    "⋯"

                Just data ->
                    data.concept
    in
    Style.marginize <|
        column
            (width fill :: Style.card)
            [ row [ width fill, spacingXY 10 0 ]
                [ paragraph []
                    [ el [ Font.italic ] (Element.text (timestampToDate zone recentTran.time))
                    , el [ Font.bold ] (Element.text userText)
                    , el [] (Element.text " - ")
                    , el [ Font.italic ] (Element.text concept)
                    ]
                , transferIcon transferKind
                ]
            , row [ width fill ]
                [ el
                    [ paddingXY 10 0, width << fillPortion <| 3 ]
                    (showFiats [] printGold userNameSearcher transMoneyPerson)
                , el [ width << fillPortion <| 2, alignRight ]
                    (showFiatTotal printGold transMoneyPerson)
                ]
            ]


transferIcon : TransferKind -> Element msg
transferIcon kind =
    let
        src =
            case kind of
                Incoming ->
                    Asset.src Asset.inIcon

                Outcoming ->
                    Asset.src Asset.outIcon
    in
    image
        [ height (px 17), width (px 17) ]
        { src = src, description = "transferIcon" }
        |> el [ alignRight, padding 4 ]


showFiatTotal : (Int -> GoldAndFiat) -> Dict String MoneyPerson -> Element msg
showFiatTotal printGold haveDict =
    haveDict
        |> Dict.values
        |> sumOfMoneyPersons
        |> .qty
        |> printGold
        |> goldFiatToFiatElement [ alignRight ]
