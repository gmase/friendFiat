module Ledger exposing (Ledger, MoneyPersonsWithTotal, decodeLedger, decodeManyLedger, emptyLedger, emptyMoneyPersonWithTotal, moneyDecoder, moneyPersonsWithTotalDecoder)

import Json.Decode as Decode exposing (int, list, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)
import Money exposing (Money, MoneyPerson)


type alias MoneyPersonsWithTotal =
    { total : Money
    , moneyPersons : List MoneyPerson
    }


moneyPersonsWithTotalDecoder : Decode.Decoder MoneyPersonsWithTotal
moneyPersonsWithTotalDecoder =
    succeed MoneyPersonsWithTotal
        |> required "total" int
        |> required "moneyPersons" (list moneyDecoder)


emptyMoneyPersonWithTotal : MoneyPersonsWithTotal
emptyMoneyPersonWithTotal =
    { total = 0
    , moneyPersons = []
    }


type alias RecentTran =
    { transId : String
    , time : Int
    , kind : String
    }


type alias Ledger =
    { id : String
    , owe : Money
    , haveTotal : Money
    , have : List MoneyPerson
    , lastTrans : List RecentTran
    }


emptyLedger : Ledger
emptyLedger =
    { id = ""
    , owe = 0
    , haveTotal = 0
    , have = []
    , lastTrans = []
    }


moneyDecoder : Decode.Decoder MoneyPerson
moneyDecoder =
    succeed MoneyPerson
        |> required "fiat" string
        |> required "qty" int


recentTranDecoder : Decode.Decoder RecentTran
recentTranDecoder =
    succeed RecentTran
        |> required "transId" string
        |> required "time" int
        |> required "kind" string


ledgerDecoder : Decode.Decoder Ledger
ledgerDecoder =
    succeed Ledger
        |> required "id" string
        |> required "owe" int
        |> required "haveTotal" int
        |> optional "have" (list moneyDecoder) []
        |> optional "lastTrans" (list recentTranDecoder) []


decodeLedger : Decode.Value -> Maybe Ledger
decodeLedger json =
    case Decode.decodeValue ledgerDecoder json of
        Ok value ->
            Just value

        Err _ ->
            Nothing


decodeManyLedger : Decode.Value -> List Ledger
decodeManyLedger json =
    case Decode.decodeValue (list ledgerDecoder) json of
        Ok value ->
            value

        Err _ ->
            []
