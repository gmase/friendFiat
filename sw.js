import { precacheAndRoute } from "workbox-precaching/precacheAndRoute";
importScripts("https://www.gstatic.com/firebasejs/8.2.10/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/8.2.10/firebase-messaging.js"
);
import {
  firebaseConfig as config,
  url as appUrl
} from "./src/firebase/config.js";

firebase.initializeApp(config);
if (firebase.messaging.isSupported()) {
  const messaging = firebase.messaging();
  messaging.onBackgroundMessage(payload => {
    const notificationTitle = payload.data.title;
    const badgeIcon =
      payload.data.kind === "target" ? "./coins.png" : "./coinAsk.png";
    const notificationOptions = {
      body: payload.data.body,
      data: payload.data,
      icon: "./favicon.png",
      badge: badgeIcon,
      vibrate: [100]
    };
    self.registration.showNotification(notificationTitle, notificationOptions);
  });

  self.addEventListener("notificationclick", event => {
    event.notification.close();
    let url =
      appUrl +
      (event.notification.data.kind === "target" ? "/balance" : "/request");
    event.waitUntil(clients.openWindow(url));
  });
} else {
  console.log("firebase messaging not supported!");
}

precacheAndRoute(self.__WB_MANIFEST);
