module UserUnit exposing (..)

import Dict
import Expect
import Ledger
import Test exposing (..)
import User


doTest1 : Test
doTest1 =
    test "Sort friends by money relationship" <|
        \_ ->
            List.sortWith (User.userSorter testingUserId balanceDict) friends
                |> Expect.equal sortedFriends



-- TEST DATA


balanceDict =
    let
        dummyLine =
            { id = "1"
            , owe = 0
            , haveTotal = 0
            , have = []
            , lastTrans = []
            }
    in
    [ ( "2", { dummyLine | have = [ { fiat = "1", qty = 8 } ] } )
    , ( "3", { dummyLine | have = [ { fiat = "1", qty = 5 } ] } )
    , ( "4", { dummyLine | have = [ { fiat = "1", qty = 12 } ] } )
    , ( "5", { dummyLine | have = [ { fiat = "1", qty = 0 } ] } )
    , ( "6", dummyLine )
    ]
        |> Dict.fromList


testingUserId : String
testingUserId =
    "1"


friends : List User.Friend
friends =
    let
        dummyFriend =
            User.getFriend "TODO"
    in
    [ { dummyFriend | id = "2" }
    , { dummyFriend | id = "3" }
    , { dummyFriend | id = "4" }
    , { dummyFriend | id = "5" }
    , { dummyFriend | id = "6" }
    ]


sortedFriends : List User.Friend
sortedFriends =
    case friends of
        a :: b :: c :: d :: e :: xs ->
            [ c, a, b, d, e ]

        _ ->
            []


getLedger : String -> Int -> Ledger.Ledger
getLedger id qty =
    let
        moneyP =
            { fiat = id, qty = qty }
    in
    { id = id
    , owe = 5
    , haveTotal = 10
    , have = [ moneyP ]
    , lastTrans = []
    }
