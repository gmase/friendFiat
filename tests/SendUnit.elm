module SendUnit exposing (..)

import Expect
import Send
import Test exposing (..)


filterContactsTest : Test
filterContactsTest =
    test "Get only money I can send to target" <|
        \_ ->
            Send.filterContacts getHave1 "person2" getFriends1
                |> Expect.equal
                    [ { fiat = "person2"
                      , qty = 5
                      }
                    , { fiat = "person1"
                      , qty = 1
                      }
                    , { fiat = "person5"
                      , qty = 22
                      }
                    ]

getHave1 =
    [ { fiat = "person1"
      , qty = 1
      }
    , { fiat = "person2"
      , qty = 5
      }
    , { fiat = "person4"
      , qty = 10
      }
    , { fiat = "person5"
      , qty = 22
      }
    ]


getFriends1 =
    [ "person1", "person2", "person3", "person5" ]


-- decodeManyTransTest : Test
-- decodeManyTransTest =
--     test "Check trans dict decoder" <|
--         \_ ->
--             decodeManyTrans getEncodedTrans
--                 |> Expect.equal
--                    []
-- TEST DATA
-- getEncodedTrans : Decode.Value
-- getEncodedTrans =
--     Encode.list
--         Encode.object
--             [
--              [
--               ("addFriendSender"
--               , Encode.string "true"
--               )
--              ,("concept"
--               , Encode.string "concepto"
--               )
--              ,("createdAt"
--               , Encode.int "1597348495928"
--               )
--              ,("from"
--               , Encode.string "1cryovRsrJceRh5z4Z9Qk9MBVDk2"
--               )
--              ,("to"
--               , Encode.string "1cryovRsrJceRh5z4Z9Qk9MBVDk2"
--               )
--              ,("status"
--               , Encode.string "payed"
--               )
--              ,("id"
--               , Encode.string "-MEdNw5rQGZNjsbmIq0s"
--               )
--              ,("amount"
--               , Encode.string "-MEdNw5rQGZNjsbmIq0s"
--               )
--                  ]
--   ]
-- addFriendSender: true
-- amount: {FIeHipGUZ4exbzf9J9ZlW8eJgJq2: 30}
-- concept: ""
-- createdAt: 1597348495928
-- from: "1cryovRsrJceRh5z4Z9Qk9MBVDk2"
-- id: "-MEdNw5rQGZNjsbmIq0s"
-- kind: "target"
-- payedAt: 1597348497817
-- status: "payed"
-- to: "FIeHipGUZ4exbzf9J9ZlW8eJgJq2"


